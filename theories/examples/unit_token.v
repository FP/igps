From iris.proofmode Require Import tactics.
From iris.algebra Require Import excl.
From iris.base_logic.lib Require Import own.

From igps Require Import viewpred.

Class uTokG Σ := UnitTokG { init_tokG :> inG Σ (exclR unitC) }.
Definition uTokΣ : gFunctors := #[GFunctor (constRF (exclR unitC))].

Instance subG_uTokΣ {Σ} : subG uTokΣ Σ → uTokG Σ.
Proof. solve_inG. Qed.

Section UnitToken.
  Context `{!uTokG Σ}.

  Set Default Proof Using "Type".

  Local Notation iTok γ := (own γ (Excl ())).

  Lemma unit_tok_exclusive γ: iTok γ ∗ iTok γ ⊢ False.
  Proof.
    iIntros "excl". rewrite -own_op.
    iDestruct (own_valid with "excl") as %Valid.
    by apply (excl_exclusive (Excl ())) in Valid.
  Qed.

End UnitToken.

Global Notation Tok γ := (☐ (own γ (Excl ())))%VP.
From mathcomp Require Import ssreflect.
From Coq Require Import ZArith.
From Coq Require Import Psatz.

Ltac lia' :=
 let rec go :=
     try match goal with
         | [ H : _ |- _ ] =>
           rewrite Nat2Pos.id in H
           || rewrite Pos2Nat.id in H
           || rewrite Z2Pos.id in H
           || rewrite Pos2Z.id in H
           || rewrite Z2Nat.id in H
           || rewrite Nat2Z.id in H
           || rewrite Nat2Pos.id_max in H;
           go
         end;
     rewrite ?Nat2Pos.id ?Pos2Nat.id ?Z2Nat.id ?Nat2Z.id ?Nat2Pos.id_max;
     lia in
 go.

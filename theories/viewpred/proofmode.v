From iris.base_logic.lib Require Import iprop.
From iris.base_logic Require Import base_logic.
From iris.proofmode Require Export tactics.
From igps Require Import viewpred.

Section Instances.
  Context {Σ : gFunctors}.
  Import uPred.
  Local Notation iProp := (iProp Σ).
  Local Notation vPred := (@vPred Σ).
  Global Instance from_and_and (P1 P2 : vPred) : ∀ V, FromAnd true ((P1 ∧ P2)%VP V) (P1 V) (P2 V).
  Proof. move => V. unfold FromAnd. done. Qed.

  Class AutoExt (P : Type) := auto_ext : P.
  Global Instance Frame_vPred_ctx p (P : vPred) :
    AutoExt (V ⊑ V') →
    (* Frame (P V') (Q V') R → *)
    Frame p (P V) (P V') (True) | 20.
  Proof.
    intros. unfold Frame. iIntros "[? R]". rewrite (@auto_ext _ X).
    destruct p; by iFrame.
  Qed.
  Global Instance Frame_vPred p (P : vPred) :
    (* Frame (P V') (Q V') R → *)
    (* class_instances.MakeSep ⌜V ⊑ V'⌝ R T → *)
    Frame p (P V) (P V') ⌜V ⊑ V'⌝ | 30.
  Proof. intros. unfold Frame. iIntros "[? %]". rewrite H. by destruct p. Qed.
End Instances.
Hint Extern 10 (AutoExt (?V1 ⊑ ?V2)) =>
(unify V1 V2; fail 2) || unfold AutoExt; solve_jsl : typeclass_instances.

Ltac iViewCnt fn :=
      let V := fresh "V" in
              let HV := fresh "HV" in
              iIntros (V HV); cbn in HV;
              fn V HV.
Ltac iView := iViewCnt ltac:(fun _ _ => idtac).
Ltac iViewUp :=
  iViewCnt ltac:(fun V HV =>
              let X := fresh "X" in
              match type of HV with
              | ?V_old ⊑ V => rewrite ?[in X in X ⊢ _](vPred_mono _ _ HV); try (clear V_old HV; rename V into V_old)
              end).
From iris.program_logic Require Import weakestpre.
From igps Require Import lang viewpred.
From igps.base Require Import ghosts.

(* Section WeakestPre. *)
(*   Context `{foundationG Σ}. *)
(*   Notation iProp := (iProp Σ). *)
(*   Notation vPred := (@vPred Σ). *)

(*   Definition vPred_wp_aux E (e : ra_lang.expr) (Φ : base.val -> vPred) : View -> iProp := *)
(*     (↑ (λ V, ∀ π Ψ, *)
(*            PSCtx *)
(*              -∗ Seen π V *)
(*              -∗ (∀ v V', ⌜V ⊑ V'⌝ -∗ Seen π V' -∗ Φ v V' -∗ Ψ (v, snd e)) *)
(*              -∗ WP e @ E {{ Ψ }} *)
(*        )%I). *)
(*   Definition vPred_wp_mono E e Φ : vPred_Mono (vPred_wp_aux E e Φ) := vProp_upclose_mono _. *)
(*   Canonical Structure vPred_wp E e Φ : vPred := *)
(*     Build_vPred _ (vPred_wp_mono E e Φ). *)

(*   (* Definition vPred_hoare_aux E e Φ P := *) *)
(*   (*   vPred_ipure (∀ π V, Seen π V -∗ P V -∗ vPred_wp E (e,π) Φ V)%I. *) *)

(*   (* Lemma vwp_bind  *) *)
(*   (*       (E : coPset) (e : ra_lang.expr) *) *)
(*   (*       (K : list base.ectx_item) (Φ : _ → vPred) V: *) *)
(*   (* (WP e @ E {{ v, ∃ V', ⌜V ⊑ V'⌝ ∗ Seen (snd e) V' ∗ vPred_wp E (foldr base.fill_item (base.of_val (v.1)) K, (snd e)) Φ V' }} -∗ PSCtx -∗ WP fill K (e) @ E {{ v, ∃  V', ⌜V ⊑ V'⌝ ∗ Seen (snd e) V' ∗ Φ (v.1) V' }}). *) *)
(*   (* Proof. *) *)
(*   (*   iIntros "WP #kI". *) *)
(*   (*   destruct e. *) *)
(*   (*   etrans; [|fast_by apply (lifting.wp_bindt K)]. *) *)
(*   (*   iApply lifting.wp_thread. simpl. *) *)
(*   (*   iApply (wp_mono with "[kI WP]"); last first. *) *)
(*   (*   - iApply (wp_frame_l). iCombine "kI" "kS" as "k". by iFrame "k WP".  *) *)
(*   (*   - iIntros (?) "([#kI kS] & WP)". *) *)
(*   (*     iDestruct "WP" as (V') "(% & kS' & WP)" . *) *)
(*   (*     iSpecialize ("WP" $! V' with "[%] kI kS'"); [done|]. simpl. *) *)
(*   (*     rewrite tactics.W.fill_unfold. simpl. *) *)
(*   (*     iApply (wp_mono with "WP"). iIntros (?) "P". *) *)
(*   (*     iDestruct "P" as (V'') "(% & ? & ?)". *) *)
(*   (*     iExists V''. iFrame "∗". iPureIntro. etrans; eauto. *) *)
(*   (* Qed. *) *)

(*   (* Lemma vwp_bindt  *) *)
(*   (*       {E : coPset} {e : base.expr} {π : thread} *) *)
(*   (*       (K : list base.ectx_item) (Φ : _ → vPred) V: *) *)
(*   (* (WP ((e, V_l) : ra_lang.expr) @ E {{ v, vPred_wp E (fill K (of_val v)) Φ V }} -∗ PSCtx -∗ Seen π V -∗ WP fill K (e, V_l) @ E {{ v, ∃  V', ⌜V ⊑ V'⌝ ∗ Seen π V' ∗ Φ (v.1) V' }}). *) *)
(*   (* Proof. *) *)
(*   (*   iIntros. iApply (vwp_bind _ (e, V_l) with "[-] [#] [#]"); auto. *) *)
(*   (*   rewrite /of_val. setoid_rewrite tactics.W.fill_unfold. *) *)
(*   (*   iPoseProof (lifting.wp_thread_rev with "[-]") as "T"; [done|done]. *) *)
(*   (* Qed. *) *)

(* End WeakestPre. *)

(* Ltac vwp_bind_core K := *)
(*   lazymatch eval hnf in K with *)
(*   | [] => idtac *)
(*   | _ => etrans; [|fast_by apply (vwp_bindt K)]; *)
(*          simpl; lazy beta iota zeta delta [fill_item base.fill_item]; simpl *)
(*   end. *)

(* Tactic Notation "vwp_bind" open_constr(efoc) := *)
(*   lazymatch goal with *)
(*   | |- _ ⊢ wp ?E (?e, ?π) ?Q => reshape_expr e ltac:(fun K e' => *)
(*     match e' with *)
(*     | efoc => unify e' efoc; vwp_bind_core K *)
(*     end) || fail "wp_bind: cannot find" efoc "in" e *)
(*   | _ => fail "wp_bind: not a 'wp'" *)
(*   end. *)


(* Notation "'WP' e @ E {{ Φ } }" := (vPred_wp E e%E Φ) *)
(*   (at level 20, e, Φ at level 200, *)
(*    format "'WP'  e  @  E  {{  Φ  } }") : vPred_scope. *)
(* Notation "'WP' e {{ Φ } }" := (vPred_wp ⊤ e%E Φ) *)
(*   (at level 20, e, Φ at level 200, *)
(*    format "'WP'  e  {{  Φ  } }") : vPred_scope. *)

(* Notation "'WP' e @ E {{ v , Q } }" := (vPred_wp E e%E (λ v, Q%VP)) *)
(*   (at level 20, e, Q at level 200, *)
(*    format "'WP'  e  @  E  {{  v ,  Q  } }") : vPred_scope. *)
(* Notation "'WP' e {{ v , Q } }" := (vPred_wp ⊤ e%E (λ v, Q%VP)) *)
(*   (at level 20, e, Q at level 200, *)
(*    format "'WP'  e  {{  v ,  Q  } }") : vPred_scope. *)

Delimit Scope val_scope with Val.

(* Texan triples *)
Notation "'{{{{' P } } } } e {{{{ x .. y , 'RET' pat ; Q } } } }" :=
  (λ V_l, □ (∀ π Φ,
                vProp_upclose (λ V_l,
                               PSCtx
                                 -∗ Seen π V_l
                                 -∗ ((P V_l)%VP)%VP
                                 -∗ ▷ (vProp_upclose (λ V, ∀ x, .. (∀ y, Seen π V -∗ ((Q V)%VP)%VP -∗ Φ (pat, V)%Val) .. ) V_l)
                                 -∗ WP (e, V_l) {{ Φ }}
                              ) V_l)
  )%I
   (at level 20, x closed binder, y closed binder,
     format "{{{{  P  } } } }  e  {{{{  x .. y ,   RET  pat ;  Q } } } }") : vPred_scope.
Notation "'{{{{' P } } } } e @ E {{{{ x .. y , 'RET' pat ; Q } } } }" :=
  (λ V_l, □ (∀ π Φ,
                vProp_upclose (λ V_l,
                               PSCtx
                                 -∗ Seen π V_l
                                 -∗ ((P V_l)%VP)%VP
                                 -∗ ▷ (vProp_upclose (λ V, ∀ x, .. (∀ y, Seen π V -∗ (Q V)%VP -∗ Φ (pat, V)%Val) .. ) V_l)
                                 -∗ WP (e, V_l) @ E {{ Φ }}
           ) V_l)
  )%I
    (at level 20, x closed binder, y closed binder,
     format "{{{{  P  } } } }  e  @  E  {{{{  x .. y ,  RET  pat ;  Q } } } }") : vPred_scope.
Notation "'{{{{' P } } } } e {{{{ 'RET' pat ; Q } } } }" :=
  (λ V_l, □ (∀ π Φ,
                vProp_upclose (λ V_l,
                               PSCtx
                                 -∗ Seen π V_l
                                 -∗ ((P V_l)%VP)%VP
                                 -∗ ▷ (vProp_upclose (λ V, Seen π V -∗ (Q V)%VP -∗ Φ (pat, V)%Val) V_l)
                                 -∗ WP (e, V_l) {{ Φ }}
                              ) V_l)
  )%I
    (at level 20,
     format "{{{{  P  } } } }  e  {{{{  RET  pat ;  Q } } } }") : vPred_scope.
Notation "'{{{{' P } } } } e @ E {{{{ 'RET' pat ; Q } } } }" :=
  (λ V_l, □ (∀ π Φ,
                vProp_upclose (λ V_l,
                               PSCtx
                                 -∗ Seen π V_l
                                 -∗ ((P V_l)%VP)%VP
                                 -∗ ▷ (vProp_upclose (λ V, Seen π V -∗ (Q V)%VP -∗ Φ (pat, V)%Val) V_l)
                                 -∗ WP (e, V_l) @ E {{ Φ }}
                              ) V_l)
  )%I
   (at level 20,
    format "{{{{  P  } } } }  e  @  E  {{{{  RET  pat ;  Q } } } }") : vPred_scope.


Notation "'{{{{' P } } } } e {{{{ x .. y , 'RET' pat ; Q } } } }" :=
  (∀ V_l π, ∀ Φ : _ → iProp _,
        vProp_upclose (λ V_l,
                       PSCtx
                         -∗ Seen π V_l
                         -∗ (P V_l)%VP
                         -∗ ▷ (vProp_upclose (λ V, ∀ x, .. (∀ y, Seen π V -∗ (Q V)%VP -∗ Φ (pat, V)%Val) .. ) V_l)
                         -∗ WP (e, V_l) {{ Φ }}
                      )%I V_l
  )
    (at level 20, x closed binder, y closed binder,
     format "{{{{  P  } } } }  e  {{{{  x .. y ,  RET  pat ;  Q } } } }") : C_scope.
Notation "'{{{{' P } } } } e @ E {{{{ x .. y , 'RET' pat ; Q } } } }" :=
  (∀ V_l π, ∀ Φ : _ → iProp _,
        vProp_upclose (λ V_l,
                       PSCtx
                         -∗ Seen π V_l
                         -∗ ((P V_l)%VP)%VP
                         -∗ ▷ (vProp_upclose (λ V, ∀ x, .. (∀ y, Seen π V -∗ (Q V)%VP -∗ Φ (pat, V)%Val) .. ) V_l)
                         -∗ WP (e, V_l) @ E {{ Φ }}
                      )%I V_l
  )
    (at level 20, x closed binder, y closed binder,
     format "{{{{  P  } } } }  e  @  E  {{{{  x .. y ,  RET  pat ;  Q } } } }") : C_scope.
Notation "'{{{{' P } } } } e {{{{ 'RET' pat ; Q } } } }" :=
  (∀ V_l π, ∀ Φ : _ → iProp _,
        vProp_upclose (λ V_l,
                       PSCtx
                         -∗ Seen π V_l
                         -∗ ((P V_l)%VP)%VP
                         -∗ ▷ (vProp_upclose (λ V, Seen π V -∗ (Q V)%VP -∗ Φ (pat, V)%Val) V_l)
                         -∗ WP (e, V_l) {{ Φ }}
                      )%I V_l
  )
    (at level 20,
     format "{{{{  P  } } } }  e  {{{{  RET  pat ;  Q } } } }") : C_scope.
Notation "'{{{{' P } } } } e @ E {{{{ 'RET' pat ; Q } } } }" :=
  (∀ V_l π, ∀ Φ : _ → iProp _,
        vProp_upclose (λ V_l,
                       PSCtx
                         -∗ Seen π V_l
                         -∗ ((P V_l)%VP)%VP
                         -∗ ▷ (vProp_upclose (λ V, Seen π V -∗ (Q V)%VP -∗ Φ (pat, V)%Val) V_l)
                         -∗ WP (e, V_l) @ E {{ Φ }}
                      )%I V_l
  )
    (at level 20,
     format "{{{{  P  } } } }  e  @  E  {{{{  RET  pat ;  Q } } } }") : C_scope.
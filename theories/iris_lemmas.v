From iris.base_logic Require Import iprop big_op.
From iris.algebra Require Import gmap.
From iris.proofmode Require Import tactics coq_tactics intro_patterns.

Section Extra.
  Context {Σ: gFunctors}.

  Lemma big_sepS_dup  `{Countable T} (S: gset T) (s: T) (φ: T → iProp Σ)
    (In: s ∈ S)
    : (φ s -∗ φ s ∗ φ s) ∗ ([∗ set] s' ∈ S, φ s')
      ⊢ φ s ∗ ([∗ set] s' ∈ S, φ s').
  Proof.
    iIntros "(Dup & ress)".
    rewrite {1}(big_sepS_delete _ _ _ In).
    iDestruct "ress" as "(Hφd & ress)".
    iDestruct ("Dup" with "Hφd") as "(Hφd1 & ?)".
    iCombine "Hφd1" "ress" as "ress".
    rewrite -(big_sepS_delete _ _ _ In).
    by iFrame.
  Qed.

  Lemma big_sepS_dup_later `{Countable T} (S: gset T) (s: T) (φ: T → iProp Σ)
    (In: s ∈ S)
    : ▷ (φ s -∗ φ s ∗ φ s) ∗ ▷ ([∗ set] s' ∈ S, φ s')
      ⊢ ▷ φ s ∗ ▷ ([∗ set] s' ∈ S, φ s').
  Proof.
    iIntros "(Dup & ress)". iNext.
    by iApply (big_sepS_dup with "[$Dup $ress]").
  Qed.

End Extra.
From iris.program_logic Require Export weakestpre ownp.
From iris.program_logic Require Import ectx_lifting.
From iris.proofmode Require Import tactics.

Import uPred.

From igps Require Import types.
From igps Require Export lang history tactics.

Set Bullet Behavior "Strict Subproofs".

Local Hint Extern 0 (atomic _) => solve_atomic.
Local Hint Resolve to_of_val.

Section lifting.
Context {Σ: gFunctors} {Iris : ownPG ra_lang Σ}.
Local Set Default Proof Using "Σ Iris".
Implicit Types P Q : iProp Σ.
Implicit Types Φ : val → iProp Σ.
Implicit Types ef : option (expr).

Lemma wp_lift_atomic_step2 {E Φ} e1 σ1 :
  strongly_atomic e1 →
  reducible e1 σ1 →
  ▷ ownP σ1 ∗
  ▷ (∀ v2 σ2 efs,
    ⌜prim_step e1 σ1 (of_val v2) σ2 efs⌝ ∧ ownP σ2 -∗ 
      |==> (|={E}=> Φ v2) ∗ [∗ list] ef ∈ efs, WP ef {{ _, True }})
  ⊢ WP e1 @ E {{ Φ }}.
Proof.
  iIntros (Hatomic ?) "[Hσ H]".
  iApply (ownP_lift_step E _ e1).
  iMod (fupd_intro_mask' E ∅) as "Hclose"; first set_solver+. iModIntro.
  iExists σ1. iFrame "Hσ"; iSplit; eauto.
  iNext; iIntros (e2 σ2 efs) "% oP".
  edestruct (Hatomic σ1 e2 σ2 efs) as [v2 <-%of_to_val]; eauto.
  iDestruct ("H" $! v2 σ2 efs with "[$oP]") as ">[HΦ $]"; first by auto.
  iMod "Hclose". iMod "HΦ". by iApply wp_value'.
Qed.

Lemma wp_lift_atomic_head_step2 {E Φ} e1 σ1 :
  strongly_atomic e1 →
  head_reducible e1 σ1 →
  ▷ ownP σ1 ∗
  ▷ (∀ v2 σ2 efs,
    ⌜ectx_language.head_step e1 σ1 (of_val v2) σ2 efs⌝ ∧ ownP σ2 -∗
      |==> (|={E}=> Φ v2) ∗ [∗ list] ef ∈ efs, WP ef {{ _, True }})
  ⊢ WP e1 @ E {{ Φ }}.
Proof.
  iIntros (??) "[? H]". iApply wp_lift_atomic_step2; eauto.
  - by eapply head_prim_reducible.
  - iFrame. iNext.
    iIntros (???) "[% ?]". iApply "H". iFrame.
    iPureIntro. by apply head_reducible_prim_step.
Qed.

(** Bind. This bundles some arguments that wp_ectx_bind leaves as indices. *)
Lemma wp_bind {E e} K Φ :
  WP e @ E {{ v, WP fill K (of_val v) @ E {{ Φ }} }} ⊢ WP fill K e @ E {{ Φ }}.
Proof. exact: wp_ectx_bind. Qed.

Lemma wp_bindt {E e π} K Φ :
  WP (e, π) @ E {{ v, WP fill K (of_val v) @ E {{ Φ }} }} ⊢ WP fill K (e, π) @ E {{ Φ }}.
Proof. exact: wp_ectx_bind. Qed.

Lemma wp_bindi {E e} Ki Φ :
  WP e @ E {{ v, WP fill_item Ki (of_val v) @ E {{ Φ }} }} ⊢
     WP fill_item Ki e @ E {{ Φ }}.
Proof. exact: weakestpre.wp_bind. Qed.

(** Base axioms for core primitives of the language: Stateless reductions *)

Local Ltac inv_head_step :=
  repeat match goal with
  | _ => progress simplify_map_eq/= (* simplify memory stuff *)
  | H : base.to_val _ = Some _ |- _ => apply base.of_to_val in H
  | H : _ = base.of_val ?v |- _ =>
     is_var v; destruct v; first[discriminate H|injection H as H]
  | H : head_step ?e _ _ _ _ |- _ =>
     try (is_var e; fail 1); (* inversion yields many goals if [e] is a variable
     and can thus better be avoided. *)
     inversion H ; subst; clear H
  | H : base.head_step ?e _ |- _ =>
     try (is_var e; fail 1); (* inversion yields many goals if [e] is a variable
     and can thus better be avoided. *)
     inversion H ; subst; clear H
  | H : step _ ?e _ _ _ |- _ =>
     try (is_var e; fail 1); (* inversion yields many goals if [e] is a variable
     and can thus better be avoided. *)
     inversion H ; subst; clear H
  end.

Local Ltac solve_exec_safe := intros; subst; do 3 eexists; do 2 econstructor; eauto.
Local Ltac solve_exec_puredet := simpl; intros; by inv_head_step.
Local Ltac solve_pure_exec :=
  repeat lazymatch goal with
  | H: IntoVal (?e,_) (?v,_) |- _ => rewrite -(base.of_to_val e v (to_base_val (e,_) (v,_) into_val)); clear H
  | H: AsRec _ _ _ _ |- _ => rewrite H; clear H
  end;
  apply det_head_step_pure_exec; [ solve_exec_safe | solve_exec_puredet ].

Global Instance pure_rec f x (erec e1 e2 : base.expr) v2 π
    `{!IntoVal (e2,π) (v2,π), AsRec e1 f x erec, base.Closed (f :b: x :b: []) erec} :
  PureExec True (App e1 e2, π) (subst' x e2 (subst' f e1 erec), π).
Proof. solve_pure_exec. by rewrite base.to_of_val. Qed.

Lemma wp_rec E π f x erec e1 e2 Φ :
  e1 = Rec f x erec →
  is_Some (base.to_val e2) →
  base.Closed (f :b: x :b: []) erec →
  ▷ WP (subst' x e2 (subst' f e1 erec), π) @ E {{ Φ }} ⊢ WP (App e1 e2, π) @ E {{ Φ }}.
Proof.
  intros ? [v2 Hv2] ?. eapply wp_pure_step_later; [eapply pure_rec; eauto|done].
  by apply to_base_val_inv.
Qed.

Lemma wp_un_op E π op l l' Φ :
  un_op_eval op l = Some l' →
  ▷ Φ (LitV l', π) ⊢ WP (UnOp op (Lit l), π) @ E {{ Φ }}.
Proof.
  intros.
   rewrite -(ownP_lift_pure_det_head_step (UnOp op _, _) (Lit l', π) [])
    ?wp_value' ; eauto.
  - by repeat econstructor.
  - intros; by inv_head_step.
Qed.

Lemma wp_bin_op E π op l1 l2 l' Φ :
  bin_op_eval op l1 l2 = Some l' →
  ▷ Φ (LitV l', π) ⊢ WP (BinOp op (Lit l1) (Lit l2), π) @ E {{ Φ }}.
Proof.
  intros Heval. rewrite -(ownP_lift_pure_det_head_step (BinOp op _ _, _) (Lit l', π) [])
    ?wp_value'; eauto.
  - by repeat econstructor.
  - intros. by inv_head_step.
Qed.

Lemma wp_if_true E π e1 e2 Φ :
  ▷ WP (e1, π) @ E {{ Φ }} ⊢ WP (If (Lit true) e1 e2, π) @ E {{ Φ }}.
Proof.
  rewrite -(ownP_lift_pure_det_head_step (If _ _ _, _) (e1, _) []) //; intros.
  - rewrite big_sepL_nil. iIntros "$".
  - by repeat econstructor.
  - by inv_head_step.
Qed.

Lemma wp_if_false E π e1 e2 Φ :
  ▷ WP (e2, π) @ E {{ Φ }} ⊢ WP (If (Lit false) e1 e2, π) @ E {{ Φ }}.
Proof.
  rewrite -(ownP_lift_pure_det_head_step (If _ _ _, _) (e2, _) []) //; intros.
  - rewrite big_sepL_nil. iIntros "$".
  - by repeat econstructor.
  - by inv_head_step.
Qed.

(** Base axioms for core primitives of the language: Stateful reductions. *)

Inductive Fork_post (σ σ' : state) (V V': View) : Prop :=
| ForkPost
    (HV : V' = V)
    (Hstate : σ' = σ)
.

Lemma wp_fork_help {e1 e2 π K} :
  (Fork e1, π) = fill K e2 → K = [].
Proof.
  move : e2. induction K as [|fi K]; first done.
  move => e2. simpl => EH.
  specialize (IHK _ EH). subst K. simpl in EH.
  destruct fi; done.
Qed.

Lemma wp_fork_pst E (σ: state) (V V': View) e Φ :
  phys_inv σ → view_ok (mem σ) V →
    ▷ ownP σ
    ∗ ▷(∀ (σ': state) V',
          ⌜phys_inv σ'⌝ ∧ ⌜rel_inv σ σ'⌝ ∧ ⌜view_ok (mem σ') V'⌝
        ∧ ⌜Fork_post σ σ' V V'⌝
        ∧ ownP σ' -∗ |==> ((|={E}=> Φ (LitV $ LitUnit, V')) ∗ WP (e, V') {{ _, True }}))
  ⊢ WP (Fork e, V) @ E {{ Φ }}.
Proof.
  intros Inv VOk.
  iIntros "[HP HΦ]".
  iApply (wp_lift_atomic_head_step2 (Fork _, _) σ).
  - induction 1 as [? ? ? ? ? Step].
    have ? := (wp_fork_help H). subst; simpl in *; simplify_eq.
    inv_head_step. eauto.
  - eexists _, σ, _. econstructor 2.
    + by econstructor.
    + by eright.
  - iSplitL "HP"; first by [].
    iNext. iIntros "% % % [% HP]". destruct a.
    inv_head_step.
    assert (Inv' : phys_inv a0) by exact: (proj1 (machine_red_safe _ _ _)).
    inversion MachStep; subst; first by inversion ThreadRed.
    simplify_eq.
    iDestruct ("HΦ" with "[$HP]") as ">[$ $]"; last done.
    iFrame (Inv'). inversion FRed; subst. iSplit; [|iSplit]; iPureIntro.
    + by destruct (machine_red_safe Inv VOk MachStep).
    + by eapply machine_red_safe.
    + econstructor; subst; eauto.
Qed.

Inductive drf_pre (σ : state) (V: View) (l: loc) : Prop :=
  | _singleton_drf
                   otl otn
                   (Local : V !! l = otl)
                   (NATS : nats σ !! l = otn)
                   (LE : otn ⊑ otl).

Inductive drf_pre_read_na (σ : state) (V: View) (l: loc) : Prop :=
  | _singleton_drf_read_na
                            otl
                            (Local : V !! l = otl)
                            (Max : MaxTime (mem σ) l otl).

Inductive init_pre (σ : state) (V: View) (l: loc) : Prop :=
  | _singleton_init
                    (tl : positive)
                    (Local : V !! l = Some tl)
                    (Init : initialized (mem σ) l tl).

Lemma initialized_reads_value (σ : state) (l : loc) tl :
  ∀ (Init : initialized (mem σ) l tl) m,
  m ∈ msgs (mem σ)
  → mloc m = l
  → mtime m = tl
  → isval (mval m).
Proof.
  inversion 1. move => m In EqLoc EqTime.
  case H: (mval m) => //; exfalso.
  - apply/Pos.lt_nle : Local => F. apply: F.
    rewrite -EqTime. apply: MT_Some_Lt => //.
    by apply elem_of_filter.
  - destruct (t_d).
    + apply/Pos.lt_nle : (Pos.lt_trans _ _ _ Alloc0 Local) => F. apply: F.
      rewrite -EqTime. apply: MT_Some_Lt => //.
      by apply elem_of_filter.
    + apply (not_elem_of_empty (C:=gset message) m). 
      by rewrite -(MT_empty TD) !elem_of_filter.
Qed.

Open Scope positive.

Inductive read_at_post (σ σ' : state) V V' x (v : Z) : Prop :=
  | ReadAtPost
               (t' : positive)
               m
               (EqVal : mval m = VInj v)
               (EqLoc : mloc m = x)
               (In : m ∈ msgs $ mem σ)
               (Local : V !! x = Some t')
               (TimeLe : t' ⊑ mtime m)
               (VUpd : V' = V ⊔ mview m)
               (MOld : mem σ' = mem σ)
               (NATSOld : nats σ' = nats σ)
  .

Inductive read_na_post (σ σ' : state) V V' x (v : Z) : Prop :=
  | ReadNAPost
               (t' : positive)
               m
               (EqVal : mval m = VInj v)
               (EqLoc : mloc m = x)
               (In : m ∈ msgs $ mem σ)
               (Local : V !! x = Some t')
               (TimeLe : t' ⊑ mtime m)
               (VUpd : V' = V ⊔ mview m)
               (MOld : mem σ' = mem σ)
               (NATSOld : nats σ' = nats σ)
               (Max : MaxTime (mem σ) x (Some $ mtime m))
  .

Lemma wp_load_at_pst E V σ (l: loc) :
  phys_inv σ →
  view_ok (mem σ) V →
  drf_pre σ V l →
  init_pre σ V l  →
    {{{ ▷ ownP σ }}}
      (Load at_hack (Lit l), V) @ E 
    {{{ v σ' V', RET (LitV v, V');
        ⌜phys_inv σ'⌝ ∧ ⌜rel_inv σ σ'⌝ ∧ ⌜view_ok (mem σ') V'⌝
        ∧ ⌜read_at_post σ σ' V V' l v⌝ ∧ ownP σ' }}}.
Proof.
  intros Inv VOk A B Φ.
  iIntros "HP HΦ".
  iApply (ownP_lift_atomic_head_step (Load at_hack _, _) σ); eauto.
  - inversion A; inversion B; simplify_option_eq.
    subst.
    move: (VOk _ _ Local0) => [m [In [EqLoc [EqTime SubView]]]].
    assert (IsVal := initialized_reads_value _ _ _ Init m In EqLoc EqTime).
    case HVal: (mval m) => [| |v]; try (by rewrite HVal in IsVal; inversion IsVal).
    eexists.
    exists σ.
    econstructor. eright; first by constructor.
    econstructor; try eauto.
    + move => ? [<-]. by econstructor.
    + econstructor => //. by rewrite -Local0.
    + simpl. assert (proof := Th_Read V (mem σ) m).
      rewrite -> HVal, EqTime, EqLoc in proof.
      eapply proof => //.
      intros. by exists tl.
  - iSplitL "HP"; first by [].
    iNext. iIntros "% % % % HP". destruct a. inv_head_step.
    assert (Inv' : phys_inv a0) by (eapply machine_red_safe; eauto).
    inversion MachStep; last by (subst; discriminate).
    subst. iSplit; last done.
    inversion ThreadRed. subst.
    iApply "HΦ". iFrame "HP". iSplit; [|iSplit; [|iSplit]]; try by []; iPureIntro.
    + rewrite /rel_inv NoWriteNats //.
      by destruct (machine_red_safe Inv VOk MachStep) as (?&?&?&?&?).
    + by destruct (machine_red_safe Inv VOk MachStep) as (?&?&?&?&?).
    + destruct Lt as [t' [Lt ?]].
      inversion DRFRed. subst.
      econstructor; subst; eauto.
      rewrite LV in Lt. by simplify_option_eq.
Qed.

Lemma wp_load_na_pst E V σ l:
  phys_inv σ →
  view_ok (mem σ) V →
  drf_pre_read_na σ V l →
  init_pre σ V l  →
    {{{ ▷ ownP σ }}}
      (Load na (Lit l), V) @ E
    {{{ v σ' V', RET (LitV  v, V');
         ⌜phys_inv σ'⌝ ∧ ⌜rel_inv σ σ'⌝ ∧ ⌜view_ok (mem σ') V'⌝
         ∧ ⌜read_na_post σ σ' V V' l v⌝ ∧ ownP σ' }}}.
Proof.
  intros Inv VOk A B Φ.
  iIntros "HP HΦ".
  iApply (ownP_lift_atomic_head_step (Load na _, _) σ); eauto.
  - inversion A; inversion B; simplify_option_eq.
    move: (VOk _ _ Local0) => [m [In [EqLoc [EqTime SubView]]]].
    assert (IsVal := initialized_reads_value _ _ _ Init m In EqLoc EqTime).
    case Hmv: (mval m) => [| |v];
      try (by rewrite Hmv in IsVal; inversion IsVal).
    eexists.
    exists σ.
    econstructor. eright; first by constructor.
    econstructor; try eauto.
    + move => ? [<-]. by econstructor.
    + econstructor => //. by rewrite -Local0.
    + simpl. assert (proof := Th_Read V (mem σ) m).
      rewrite -> Hmv, EqTime, EqLoc in proof.
      eapply proof => //. exists tl => //.
  - iSplitL "HP"; first by [].
    iNext. iIntros "% % % % HP". destruct a. inv_head_step.
    assert (Inv' : phys_inv a0) by exact: (proj1 (machine_red_safe _ _ _)).
    inversion MachStep; last by (subst; discriminate).
    subst. iSplit; last done.
    inversion ThreadRed. subst.
    iApply "HΦ". iFrame "HP". iSplit; [|iSplit; [|iSplit]]; try by []; iPureIntro.
    + rewrite /rel_inv NoWriteNats //.
      by destruct (machine_red_safe Inv VOk MachStep) as (?&?&?&?&?).
    + by destruct (machine_red_safe Inv VOk MachStep) as (?&?&?&?&?).
    + destruct Lt as [t' [Lt ?]].
      inversion DRFRed. rewrite Lt in LV. simplify_option_eq.
      econstructor; subst; eauto.
Qed.

Inductive alloc_pre (σ : state) (V: View) (l: loc) : Prop :=
  | _singleton_alloc
                    (tl : positive)
                    (Local : V !! l = Some tl)
                    (Alloc : allocated (mem σ) l tl).

Inductive write_at_post (σ σ' : state) V V' x (v : Z) : Prop :=
  | WriteAtPost
               m
               (EqVal : mval m = VInj v)
               (EqLoc : mloc m = x)
               (EqView: mview m = V')
               (Local : V !! x ⊏ Some (mtime m))
               (VUpd  : V' = V ⊔ {[x := mtime m]})
               (NATSOld : nats σ' = nats σ)
               (Disj : MS_msg_disj (msgs $ mem σ) m)
               (MUpd : mem σ' = add_ins (mem σ) m Disj)
  .

Lemma wp_store_at_pst E V σ l v:
  phys_inv σ →
  view_ok (mem σ) V →
  drf_pre σ V l →
  alloc_pre σ V l  →
   {{{ ▷ ownP σ }}}
    (Store at_hack (Lit l) (Lit v), V) @ E
   {{{ σ' V', RET (LitV LitUnit, V');
        ⌜phys_inv σ'⌝ ∧ ⌜rel_inv σ σ'⌝ ∧ ⌜view_ok (mem σ') V'⌝
        ∧ ⌜write_at_post σ σ' V V' l v⌝ ∧ ownP σ' }}}.
Proof.
  intros Inv VOk A B Φ.
  iIntros "HP HΦ".
  iApply (ownP_lift_atomic_head_step (Store _ _ _, _) σ); eauto.
  - inversion A; inversion B; simplify_option_eq.
    subst.
    move: (VOk _ _ Local0) => [ml [In [EqLoc [EqTime SubView]]]].
    assert (NE: hist (mem σ) l ≢ ∅).
      { by apply (non_empty_inhabited ml), elem_of_hist. }
    destruct (hist_max_time _ _ NE) as [m' [Inm' tMax]].
    pose (m := <l → (VInj v) @ (mtime m' + 1), (V ⊔ {[l := mtime m' + 1]})>).
    assert (Disj: MS_msg_disj (msgs $ mem σ) m).
      { move => m0 HIn.
        case (decide (mloc m = mloc m0)) => [LEq|LNEq];
        last by left. right. rewrite /m /= => Eq.
        apply (Pos.lt_irrefl (mtime m0)).
        rewrite -{2}Eq. eapply Pos.le_lt_trans; last by apply Pos.lt_add_r.
        by apply tMax, elem_of_hist. }
    assert (V !! l ⊏ Some (mtime m' + 1)).
      { rewrite Local0 -EqTime.
        eapply Pos.le_lt_trans; last by apply Pos.lt_add_r.
        by apply tMax, elem_of_hist. }

    eexists.
    exists (mkState 
                    (add_ins (mem σ) m Disj)
                    (nats σ)).
    econstructor. eright; first by constructor.
    econstructor; try eauto => //.
    + move => ? [<-]. by econstructor.
    + econstructor => //. by rewrite -Local0.
    + simpl. econstructor; eauto.
  - iSplitL "HP"; first by [].
    iNext. iIntros "% % % % HP". destruct a as [v' π']. inv_head_step.
    assert (Inv' : phys_inv a0) by exact: (proj1 (machine_red_safe _ _ _)).
    inversion MachStep; last by (subst; discriminate).
    subst. iSplit; last done.
    inversion ThreadRed. subst. simplify_option_eq.
    iApply "HΦ". iFrame "HP". iSplit; [|iSplit; [|iSplit]]; try by []; iPureIntro.
    + rewrite /rel_inv /=.
      by edestruct (machine_red_safe Inv VOk MachStep) as (?&?&?&?&?).
    + by edestruct (machine_red_safe Inv VOk MachStep) as (?&?&?&?&?).
    + econstructor; subst; eauto; simpl; eauto.
      by inversion DRFRed.
Qed.


Inductive write_na_post (σ σ' : state) V V' x (v : Z) : Prop :=
  | WriteNAPost
               m
               (EqVal : mval m = VInj v)
               (EqLoc : mloc m = x)
               (EqView: mview m = V')
               (Local : V !! x ⊏ Some (mtime m))
               (VUpd  : V' = V ⊔ {[x := mtime m]})
               (NATSUpd : nats σ' = <[x := mtime m]>(nats σ))
               (Disj : MS_msg_disj (msgs $ mem σ) m)
               (MUpd : mem σ' = add_ins (mem σ) m Disj)
               (Max : MaxTime (mem σ') x (Some $ mtime m))
  .

Lemma wp_store_na_pst E σ V l v:
  phys_inv σ →
  view_ok (mem σ) V →
  drf_pre σ V l →
  alloc_pre σ V l  →
    {{{ ▷ ownP σ }}}
      (Store na (Lit l) (Lit v), V) @ E
    {{{ σ' V', RET (LitV (LitUnit), V');
          ⌜phys_inv σ'⌝ ∧ ⌜rel_inv σ σ'⌝ ∧ ⌜view_ok (mem σ') V'⌝
          ∧ ⌜write_na_post σ σ' V V' l v⌝ ∧ ownP σ' }}}.
Proof.
  intros Inv VOk A B Φ.
  iIntros "HP HΦ".
  iApply (ownP_lift_atomic_head_step (Store _ _ _, _) σ); eauto.
  - inversion A; inversion B; simplify_option_eq.
    subst.
    move: (VOk _ _ Local0) => [ml [In [EqLoc [EqTime SubView]]]].
    assert (NE: hist (mem σ) l ≢ ∅).
      { by apply (non_empty_inhabited ml), elem_of_hist. }
    destruct (hist_max_time _ _ NE) as [m' [Inm' tMax]].
    pose (m := <l → (VInj v) @ (mtime m' + 1), (V ⊔ {[l := mtime m' + 1]})>).
    assert (Disj: MS_msg_disj (msgs $ mem σ) m).
      { move => m0 HIn.
        case (decide (mloc m = mloc m0)) => [LEq|LNEq];
        last by left. right. rewrite /m /= => Eq.
        apply (Pos.lt_irrefl (mtime m0)).
        rewrite -{2}Eq. eapply Pos.le_lt_trans; last by apply Pos.lt_add_r.
        by apply tMax, elem_of_hist. }
    assert (V !! l ⊏ Some (mtime m' + 1)).
      { rewrite Local0 -EqTime.
        eapply Pos.le_lt_trans; last by apply Pos.lt_add_r.
        by apply tMax, elem_of_hist. }

    eexists.
    exists (mkState (add_ins (mem σ) m Disj)
               (<[l:= mtime m]> (nats σ))).
    econstructor. eright; first by constructor.
    econstructor; try eauto => //.
    + move => ? [<-]. by econstructor.
    + econstructor; [|..|exact: H|] => //.
      apply: (MT_Some m) => //.
      * rewrite gset_filter_union elem_of_union
          !elem_of_filter elem_of_singleton; by right.
      * apply set_unfold_2.
        move => ? [/= ? [?|-> //]].
        eapply Pos.lt_le_incl, Pos.le_lt_trans; last by apply Pos.lt_add_r.
        by apply tMax, elem_of_hist.
    + by econstructor.
  - iSplitL "HP"; first by [].
    iNext. iIntros "% % % % HP". destruct a as [v' π']. inv_head_step.
    assert (Inv' : phys_inv a0) by exact: (proj1 (machine_red_safe _ _ _)).
    inversion MachStep; last by (subst; discriminate).
    subst. iSplit; last done.
    inversion ThreadRed. subst. simplify_option_eq.
    iApply "HΦ". iFrame "HP". iSplit; [|iSplit; [|iSplit]]; try by []; iPureIntro.
    + rewrite /rel_inv /=.
      by destruct (machine_red_safe Inv VOk MachStep) as (?&?&?&?&?).
    + by destruct (machine_red_safe Inv VOk MachStep) as (?&?&?&?&?).
    + inversion DRFRed.
      econstructor; subst; eauto; simpl; eauto.
Qed.


Inductive dealloc_post (σ σ' : state) V V' x : Prop :=
  | DeallocPost
               m
               (EqVal : mval m = D)
               (EqLoc : mloc m = x)
               (EqView: mview m = V')
               (Local : V !! x ⊏ Some (mtime m))
               (VUpd  : V' = V ⊔ {[x := mtime m]})
               (NATSUpd : nats σ' = <[x := mtime m]>(nats σ))
               (Disj : MS_msg_disj (msgs $ mem σ) m)
               (MUpd : mem σ' = add_ins (mem σ) m Disj)
               (Max : MaxTime (mem σ') x (Some $ mtime m))
  .

Lemma wp_dealloc_pst E V σ l:
  phys_inv σ →
  view_ok (mem σ) V →
  drf_pre σ V l →
  alloc_pre σ V l  →
    {{{ ▷ ownP σ }}}
      (Dealloc (Lit l), V) @ E
    {{{ σ' V', RET (LitV LitUnit, V');
          ⌜phys_inv σ'⌝ ∧ ⌜rel_inv σ σ'⌝ ∧ ⌜view_ok (mem σ') V'⌝
          ∧ ⌜dealloc_post σ σ' V V' l⌝ ∧ ownP σ' }}}.
Proof.
  intros Inv VOk A B Φ.
  iIntros "HP HΦ".
  iApply (ownP_lift_atomic_head_step (Dealloc _, _) σ); eauto.
  - inversion A; inversion B; simplify_option_eq.
    subst.
    move: (VOk _ _ Local0) => [ml [In [EqLoc [EqTime SubView]]]].
    assert (NE: hist (mem σ) l ≢ ∅).
      { by apply (non_empty_inhabited ml), elem_of_hist. }
    destruct (hist_max_time _ _ NE) as [m' [Inm' tMax]].
    pose (m := <l → D @ (mtime m' + 1), (V ⊔ {[l := mtime m' + 1]})>).
    assert (Disj: MS_msg_disj (msgs $ mem σ) m).
      { move => m0 HIn.
        case (decide (mloc m = mloc m0)) => [LEq|LNEq];
        last by left. right. rewrite /m /= => Eq.
        apply (Pos.lt_irrefl (mtime m0)).
        rewrite -{2}Eq. eapply Pos.le_lt_trans; last by apply Pos.lt_add_r.
        by apply tMax, elem_of_hist. }
    assert (V !! l ⊏ Some (mtime m' + 1)).
      { rewrite Local0 -EqTime.
        eapply Pos.le_lt_trans; last by apply Pos.lt_add_r.
        by apply tMax, elem_of_hist. }
    eexists.
    exists (mkState (add_ins (mem σ) m Disj)
               (<[l:=mtime m]> (nats σ))).
    econstructor. eright; first by constructor.
    econstructor; try eauto => //.
    + move => ? [<-]. by econstructor.
    + econstructor; [..|exact: H|] => //.
      apply: (MT_Some m) => //.
      * rewrite gset_filter_union elem_of_union
          !elem_of_filter elem_of_singleton; by right.
      * apply set_unfold_2.
        move => ? [/= ? [?|-> //]].
        eapply Pos.lt_le_incl, Pos.le_lt_trans; last by apply Pos.lt_add_r.
        by apply tMax, elem_of_hist.
    + by econstructor.
  - iSplitL "HP"; first by [].
    iNext. iIntros "% % % % HP". destruct a as [v' π']. inv_head_step.
    assert (Inv' : phys_inv a0) by exact: (proj1 (machine_red_safe _ _ _)).
    inversion MachStep; last by (subst; discriminate). iSplit; last done.
    inversion ThreadRed. subst.
    iApply "HΦ". iFrame "HP". iSplit; [|iSplit; [|iSplit]]; try by []; iPureIntro.
    + by destruct (machine_red_safe Inv VOk MachStep) as (?&?&?&?&?).
    + by destruct (machine_red_safe Inv VOk MachStep) as (?&?&?&?&?).
    + inversion DRFRed.
      econstructor; subst; eauto; simpl; eauto.
Qed.

Inductive CAS_succ_post (σ σ' : state) V V' x (v_r v_w: Z) : Prop :=
  | CASSuccPost
               (tl tr : positive)
               m m'
               (InR: m ∈ (msgs $ mem σ))
               (EqVal : mval m = VInj v_r)
               (EqLoc : mloc m = x)
               (EqTime: mtime m = tr)
               (EqVal' : mval m' = VInj v_w)
               (EqLoc' : mloc m' = x)
               (EqView': mview m' = V')
               (EqTime': mtime m' = tr + 1)
               (VUpd  : V' =  V ⊔ {[x := tr + 1]} ⊔ mview m)
               (Local : V !! x = Some tl)
               (TimeLe: tl ⊑ tr)
               (Disj : MS_msg_disj (msgs $ mem σ) m')
               (MUpd : mem σ' = add_ins (mem σ) m' Disj)
               (NATSOld: nats σ' = nats σ)
  .

Inductive CAS_fail_post (σ σ' : state) V V' x (v_r: Z) : Prop :=
  | CASFailPost
               (t : positive)
               m v
               (InR: m ∈ (msgs $ mem σ))
               (EqVal : mval m = VInj v)
               (NEqVal : v ≠ v_r)
               (EqLoc : mloc m = x)
               (VUpd  : V' =  V ⊔ mview m)
               (Local : V !! x = Some t)
               (TimeLe : t ⊑ mtime m)
               (MOld : mem σ' = mem σ)
               (NATSOld : nats σ' = nats σ)
  .

Lemma wp_CAS_pst E V σ l v_r v_w:
  phys_inv σ →
  view_ok (mem σ) V →
  drf_pre σ V l →
  init_pre σ V l  →
    {{{ ▷ ownP σ }}}
      (CAS (Lit l) (Lit v_r) (Lit v_w), V) @ E
    {{{ b σ' V', RET (LitV b, V');
           ⌜phys_inv σ'⌝ ∧ ⌜rel_inv σ σ'⌝ ∧ ⌜view_ok (mem σ') V'⌝
         ∧ ⌜  b = true  ∧ CAS_succ_post σ σ' V V' l v_r v_w
              ∨ b = false ∧ CAS_fail_post σ σ' V V' l v_r  ⌝
         ∧ ownP σ' }}}.
Proof.
  intros Inv VOk A B Φ.
  iIntros "HP HΦ".
  iApply (ownP_lift_atomic_head_step (CAS _ _ _, _) σ); eauto.
  - inversion A; inversion B; simplify_option_eq.
    subst.
    move: (VOk _ _ Local0) => [ml [In [EqLoc [EqTime SubView]]]].
        assert (NE: hist (mem σ) l ≢ ∅).
      { by apply (non_empty_inhabited ml), elem_of_hist. }
    destruct (hist_max_time _ _ NE) as [m' [Inm' tMax]].
    assert (V !! l ⊏ Some (mtime m' + 1)).
      { rewrite Local0 -EqTime.
        eapply Pos.le_lt_trans; last by apply Pos.lt_add_r.
        by apply tMax, elem_of_hist. }
    apply elem_of_hist in Inm' as [].
    assert (∃v, mval m' = VInj v) as [v Hv].
      { inversion Init. destruct (mval m') as [ | |v'] eqn:Hmv;
        last by exists v'.
        - exfalso; move: Local.
          rewrite <- (MT_Some_Lt TA m') => //;
            last by move : H1 Hmv; set_solver+.
          rewrite -EqTime (tMax ml _) ?elem_of_hist //. exact: Pos.lt_irrefl.
        - exfalso; move: Alloc0.
          rewrite <- (MT_max TD m') => //;
            last by move : H0 H1 Hmv;set_solver+.
          cbn.
          rewrite Local -EqTime (tMax ml _) ?elem_of_hist //.
          exact: Pos.lt_irrefl.
      }
    case (decide (v = v_r)) => [Eqv|NEqv].
    + pose (m := <l → (VInj v_w) @ (mtime m' + 1), (V ⊔ {[l := mtime m' + 1]}  ⊔ mview m')>).
      assert (Disj: MS_msg_disj (msgs $ mem σ) m).
      { move => m0 HIn.
        case (decide (mloc m = mloc m0)) => [LEq|LNEq];
        last by left. right. rewrite /m /= => Eq.
        apply (Pos.lt_irrefl (mtime m0)).
        rewrite -{2}Eq. eapply Pos.le_lt_trans; last by apply Pos.lt_add_r.
        by apply tMax, elem_of_hist. }
      eexists.
      exists (mkState (add_ins (mem σ) m Disj) 
                 (nats σ)).
      econstructor. eright; first by constructor.
      econstructor; try eauto => //=.
      * move => ? [<-]. by econstructor.
      * econstructor => //. by rewrite -Local0.
      * econstructor; eauto.
        { rewrite -Eqv -Hv -H0. by destruct m'. }
        { exists tl. split => //. rewrite -EqTime.
          by apply tMax, elem_of_hist. }
    + eexists.
      exists (mkState (mem σ) (nats σ)).
      econstructor. eright. eapply CasFailS; eauto.
      econstructor; try eauto => //=.
      * move => ? [<-]. by econstructor.
      * econstructor => //. by rewrite -Local0.
      * assert (proof := Th_Read V (mem σ) m').
        rewrite Hv H0 in proof.
        eapply proof => //.
        exists tl. split => //.
        rewrite -EqTime. by apply tMax, elem_of_hist.
  - iSplitL "HP"; first by [].
    iNext. iIntros "% % % % HP". destruct a as [v' π'].
    inversion a2; first by inversion BaseStep; clear a2. subst.
    assert (Inv' : phys_inv a0) by exact: (proj1 (machine_red_safe _ _ _)).
    inv_head_step; inversion MachStep; subst; try discriminate; (iSplitR ""; last done).
    + inversion ThreadRed; subst. simplify_option_eq.
      iApply ("HΦ" $! false with "[$HP]"). iFrame (Inv'). iSplit; [|iSplit]; iPureIntro.
      * by destruct (machine_red_safe Inv VOk MachStep) as (?&?&?&?&?).
      * by destruct (machine_red_safe Inv VOk MachStep) as (?&?&?&?&?).
      * destruct Lt as [t' [Lt1 Lt2]].
        right. split => //.
         econstructor; subst; eauto; simpl; eauto.
    + inversion ThreadRed; subst. simplify_option_eq.
      iApply ("HΦ" $! true with "[$HP]").
      iFrame (Inv'). iSplit; [|iSplit]; iPureIntro.
      * by destruct (machine_red_safe Inv VOk MachStep) as (?&?&?&?&?).
      * by destruct (machine_red_safe Inv VOk MachStep) as (?&?&?&?&?).
      * destruct Lt as [t' [Lt1 Lt2]].
        left. split =>//. econstructor; subst; eauto; simpl; eauto.
        by inversion DRFRed.
Qed.

Inductive FAI_post C (σ σ' : state) V V' x (v: Z) : Prop :=
  | FAIPost
            (tl tr : positive)
            m m'
            (InR: m ∈ (msgs $ mem σ))
            (EqVal : mval m = VInj v)
            (EqLoc : mloc m = x)
            (EqTime: mtime m = tr)
            (EqVal' : mval m' = VInj ((v + 1) `mod` Z.pos C))
            (EqLoc' : mloc m' = x)
            (EqView': mview m' = V')
            (EqTime': mtime m' = tr + 1)
            (VUpd  : V' =  V ⊔ {[x := tr + 1]} ⊔ mview m)
            (Local : V !! x = Some tl)
            (TimeLe: tl ⊑ tr)
            (Disj : MS_msg_disj (msgs $ mem σ) m')
            (MUpd : mem σ' = add_ins (mem σ) m' Disj)
            (NATSOld: nats σ' = nats σ)
  .


Lemma wp_FAI_pst C E V σ l :
  phys_inv σ →
  view_ok (mem σ) V →
  drf_pre σ V l →
  init_pre σ V l  →
    {{{ ▷ ownP σ }}}
      (FAI C (Lit l), V) @ E
    {{{ v σ' V', RET (LitV $ LitInt v, V');
          ⌜phys_inv σ'⌝ ∧ ⌜rel_inv σ σ'⌝ ∧ ⌜view_ok (mem σ') V'⌝
          ∧ ⌜FAI_post C σ σ' V V' l v⌝ ∧ ownP σ' }}}.
Proof.
  intros Inv VOk A B Φ.
  iIntros "HP HΦ".
  iApply (ownP_lift_atomic_head_step (FAI _ _, _) σ); eauto.
  - inversion A; inversion B; simplify_option_eq.
    subst.
    move: (VOk _ _ Local0) => [ml [In [EqLoc [EqTime SubView]]]].
    assert (NE: hist (mem σ) l ≢ ∅).
      { by apply (non_empty_inhabited ml), elem_of_hist. }
    destruct (hist_max_time _ _ NE) as [m' [Inm' tMax]].
    assert (V !! l ⊏ Some (mtime m' + 1)).
      { rewrite Local0 -EqTime.
        eapply Pos.le_lt_trans; last by apply Pos.lt_add_r.
        by apply tMax, elem_of_hist. }
    apply elem_of_hist in Inm' as [].
    assert (∃v, mval m' = VInj v) as [v Hv].
      { inversion Init. destruct (mval m') as [ | |v'] eqn:Hmv;
        last by exists v'.
        - exfalso; move: Local.
          rewrite <- (MT_Some_Lt TA m') => //;
            last by move : H1 Hmv; set_solver+.
          rewrite -EqTime (tMax ml _) ?elem_of_hist //. exact: Pos.lt_irrefl.
        - exfalso; move: Alloc0.
          rewrite <- (MT_max TD m') => //;
             last by move : H0 H1 Hmv; set_solver+. cbn.
          rewrite Local -EqTime (tMax ml _) ?elem_of_hist //. exact: Pos.lt_irrefl.
      }
    pose (m := <l → VInj ((v + 1) `mod` Z.pos C) @ (mtime m' + 1), (V ⊔ {[l := mtime m' + 1]}  ⊔ mview m')>).
    assert (Disj: MS_msg_disj (msgs $ mem σ) m).
      { move => m0 HIn.
        case (decide (mloc m = mloc m0)) => [LEq|LNEq];
        last by left.
        right. move => TEq. apply (Pos.lt_irrefl (mtime m)).
        apply (Pos.le_lt_trans _ (mtime m')).
        - rewrite TEq. by apply tMax, elem_of_hist.
        - apply Pos.lt_add_r. }
    eexists.
    exists (mkState (add_ins (mem σ) m Disj) 
               (nats σ)).
    econstructor. eright; first by constructor.
    econstructor; try eauto => //=.
    * move => ? [<-]. by econstructor.
    * econstructor => //. by rewrite -Local0.
    * econstructor; eauto.
      { rewrite -Hv -H0. by destruct m'. }
      { exists tl. split => //. rewrite -EqTime.
        by apply tMax, elem_of_hist. }
  - iSplitL "HP"; first by [].
    iNext. iIntros "% % % % HP". destruct a as [v' π'].
    inversion a2; first by inversion BaseStep; clear a2. subst.
    assert (Inv' : phys_inv a0) by exact: (proj1 (machine_red_safe _ _ _)).
    inv_head_step; inversion MachStep; subst; try discriminate; (iSplitR ""; last done).
    inversion ThreadRed; subst.
    iApply ("HΦ" with "[$HP]"). iFrame (Inv'). iSplit; [|iSplit]; iPureIntro.
    + by destruct (machine_red_safe Inv VOk MachStep) as (?&?&?&?&?).
    + by destruct (machine_red_safe Inv VOk MachStep) as (?&?&?&?&?).
    + destruct Lt as [t' [Lt1 Lt2]].
      econstructor; subst; eauto; simpl; eauto.
      by inversion DRFRed.
Qed.

Inductive alloc_post (σ σ' : state) V V' x : Prop :=
  | AllocPost m
               (Local : V !! x = None)
               (EqVal : mval m = A)
               (EqLoc : mloc m = x)
               (EqView : mview m = V')
               (VUpd  : V' =  V ⊔ {[x := mtime m]})
               (Disj : MS_msg_disj (mem σ) m)
               (MUpd : mem σ' = add_ins (mem σ) m Disj)
               (NATSOld: nats σ' = <[x := mtime m]>(nats σ))
               (Fresh:  filter (λ y, mloc y = x) (msgs (mem σ)) ≡ ∅)
  .

Lemma wp_alloc_pst E V σ:
  phys_inv σ →
  view_ok (mem σ) V →
    {{{ ▷ ownP σ }}}
      (Alloc, V) @ E
    {{{ l σ' V', RET (LitV l, V');
         ⌜phys_inv σ'⌝ ∧ ⌜rel_inv σ σ'⌝ ∧ ⌜view_ok (mem σ') V'⌝
         ∧ ⌜alloc_post σ σ' V V' l⌝ ∧ ownP σ' }}}.
Proof.
  intros Inv VOk Φ.
  iIntros "HP HΦ".
  iApply (ownP_lift_atomic_head_step (Alloc, _) σ); eauto.
  - destruct (fresh_location (msgs (mem σ))) as [l Fresh].
    assert (NVl: V !! l = None).
      { destruct (V !! l) eqn:TEq => //.
        destruct (VOk _ _ TEq) as [m' [In [? _]]].
        exfalso. by apply (Fresh _ In). }
    assert (nats σ !! l = None).
      { destruct (nats σ !! l) eqn:NAEq => //.
        exfalso. destruct (INOk _ Inv _ _ NAEq) as [m' [In [? _]]].
        by apply (Fresh _ In). }
    pose (m := <l → A @ 1, (V ⊔ {[l := 1]})>).
    assert (Disj: MS_msg_disj (msgs $ mem σ) m).
      { move => m0 HIn.
        case (decide (mloc m = mloc m0)) => [LEq|LNEq];
        last by left. exfalso. apply (Fresh _ HIn). rewrite /m in LEq.
        by simpl in LEq. }
    eexists.
    exists (mkState (add_ins (mem σ) m Disj) 
               (<[l := mtime m]> (nats σ))).
    econstructor. eright; first by constructor.
    econstructor; try eauto => //.
    + move => x. instantiate (1 := l). move => /= EqLoc.
      inversion EqLoc. subst x. constructor => //. move: Fresh; set_solver+.
    + instantiate (1:= Some 1). econstructor; eauto => //.
      simpl. apply: (MT_Some m) => //.
      * rewrite gset_filter_union elem_of_union.
        right. by rewrite elem_of_filter elem_of_singleton.
      * setoid_rewrite elem_of_filter.
        setoid_rewrite elem_of_union.
        setoid_rewrite elem_of_singleton.
        move => m' [? [In|Eq]].
        { exfalso. by apply (Fresh _ In). }
        { by rewrite Eq /=. }
    + constructor; eauto. by rewrite NVl.
  - iSplitL "HP"; first by [].
    iNext. iIntros "% % % % HP". destruct a as [v' π']. inv_head_step.
    assert (Inv' : phys_inv a0) by exact: (proj1 (machine_red_safe _ _ _)).
    inversion MachStep; last by (subst; discriminate). iSplit; last done.
    assert (AllocRed: alloc_red (mem σ) V l (EWrite na l A)). { by apply ARed. }
    inversion ThreadRed. subst. simplify_option_eq.
    iApply ("HΦ" with "[$HP]"). iFrame (Inv'). iSplit; [|iSplit]; iPureIntro.
    + by destruct (machine_red_safe Inv VOk MachStep) as (?&?&?&?&?).
    + by destruct (machine_red_safe Inv VOk MachStep) as (?&?&?&?&?).
    + inversion AllocRed. inversion DRFRed. subst.
      econstructor; subst; eauto; simpl; eauto.
      case Vl: (V !! _) => //.
      destruct (VOk _ _ Vl) as [m [In [EqLoc _]]].
      exfalso. apply (not_elem_of_empty (C:=gset message) m). 
      rewrite -Fresh. move: In EqLoc; set_solver+.
Qed.

Close Scope positive.
End lifting.

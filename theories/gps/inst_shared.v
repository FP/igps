From iris.base_logic.lib Require Export cancelable_invariants sts.
From iris.algebra Require Export auth.

Import uPred.

From igps Require Export persistor fractor proofmode weakestpre viewpred.
From igps Require Export notation na.
From igps.gps Require Export raw.

Definition state_sort_eqdec Prtcl {PF : protocol_facts Prtcl} : EqDecision (state_sort Prtcl) := _.
Notation gps_agreeG Σ Prtcl := (inG Σ (dec_agreeR (EqDecision0 := state_sort_eqdec Prtcl%type) (state_sort Prtcl))).
Definition gps_agreeΣ Prtcl (PF : protocol_facts Prtcl) : gFunctors := #[ GFunctor (constRF ((dec_agreeR (EqDecision0 := state_sort_eqdec Prtcl%type) (state_sort Prtcl)))) ].

Instance subG_gps_agreeΣ Σ Prtcl PF : subG (gps_agreeΣ Prtcl PF) Σ → gps_agreeG Σ Prtcl.
Proof. solve_inG. Qed.

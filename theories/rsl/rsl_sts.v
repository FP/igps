From iris.algebra Require Import sts.
From iris.base_logic.lib Require Import own.
From stdpp Require Export gmap.

From igps Require Import types infrastructure.
From igps.base Require Export ghosts.
From igps.rsl Require Export gset_iops.

Implicit Types (Γ : gset gname) (h: History) (i: gname).

(* RSL STS states contain a history h and splitting Γ:
  - raUn Γ h: the location is non-CAS and uninitialized
  - raIn Γ h: the location is non-CAS and initialized
  - rcUn Γ h: the location is CAS-able and uninitialized
  - rcIn Γ h: the location is CAS-able and initialized. *)
Inductive state_type := | raUn | raIn | rcUn | rcIn.

Record state := mkRST {
  rtype : state_type;
  rset : gset gname;
  rhist : History;
}.

Implicit Types (s: state).

Definition isNonCAS s := rtype s = raUn ∨ rtype s = raIn.
Definition isCAS    s := rtype s = rcUn ∨ rtype s = rcIn.
Definition isInit   s := rtype s = raIn ∨ rtype s = rcIn.
Definition isUninit s := rtype s = raUn ∨ rtype s = rcUn.

Inductive token := Change i.

Global Instance stateT_inhabited: Inhabited state := populate (mkRST raUn ∅ ∅).

Global Instance Change_inj : Inj (=) (=) Change.
Proof. by injection 1. Qed.

Inductive prim_step : relation state :=
  (** non-CAS location *)
  (* change the spliting, while still uninitialized *)
  | ChangeUa Γ1 Γ2 h: prim_step (mkRST raUn Γ1 h) (mkRST raUn Γ2 h)
  (* initialization write, doesn't change the spliting *)
  | ChangeSa Γ V v h
      : prim_step (mkRST raUn Γ h) (mkRST raIn Γ ({[(VInj v, V)]} ∪ h))
  (* change the spliting after initialized, doesn't change history *)
  | ChangeIa Γ1 Γ2 h
      : prim_step (mkRST raIn Γ1 h) (mkRST raIn Γ2 h)
  (* normal write after initialized, doesn't change splitting *)
  | ChangeIWa Γ V v h
      : prim_step (mkRST raIn Γ h) (mkRST raIn Γ ({[(VInj v, V)]} ∪ h))
  (** CAS-only location *)
  (* change the spliting, while still uninitialized *)
  | ChangeUc Γ1 Γ2 h: prim_step (mkRST rcUn Γ1 h) (mkRST rcUn Γ2 h)
  (* initialization write, doesn't change the spliting *)
  | ChangeSc Γ V v h
      : prim_step (mkRST rcUn Γ h) (mkRST rcIn Γ ({[(VInj v, V)]} ∪ h))
  (* change the spliting after initialized, doesn't change history *)
  | ChangeIc Γ1 Γ2 h: prim_step (mkRST rcIn Γ1 h) (mkRST rcIn Γ2 h)
  (* normal write after initialized, doesn't change splitting *)
  | ChangeIWc Γ V v h
      : prim_step (mkRST rcIn Γ h) (mkRST rcIn  Γ ({[(VInj v, V)]} ∪ h)).

Notation "'!tok' I0" := ({[ t | ∃ i0, t = Change i0 ∧ i0 ∉ I0 ]} : set token)
  (at level 40, format "!tok  I0").

Definition tok s: set token := !tok (rset s).

Global Arguments tok !_ /.

Canonical Structure rsl_sts := sts.Sts prim_step tok.

Definition i_states i : set state := {[ s | i ∈ rset s ]}.

Lemma i_states_closed i : sts.closed (i_states i) {[ Change i ]}.
Proof.
  split; first (intros [[| | |]] ; abstract set_solver+).
  intros s1 s2 Hs1 [T1 T2 Hdisj Hstep'].
  inversion_clear Hstep' as [? ? ? ? Htrans _ _ Htok].
  destruct Htrans; done || (abstract set_solver).
Qed.

Lemma i_states_in s i: i ∈ rset s → s ∈ i_states i.
Proof. done. Qed.

Lemma frame_step_step s s' T: sts.frame_step T s s' → prim_step s s'.
Proof. inversion 1 as [????Step]. by inversion Step. Qed.

Lemma frame_steps_steps s s' T : sts.frame_steps T s s' → rtc prim_step s s'.
Proof.
  induction 1; [done|]. econstructor; [|by eauto]. by eapply frame_step_step.
Qed.

Lemma frame_steps_RMW s s' T:
  sts.frame_steps T s s' → isCAS s → isCAS s'.
Proof.
  induction 1 as [|??? Step _ IH]; [done|] => Hs. apply IH. clear IH.
  inversion_clear Step as [??? Step0]. inversion_clear Step0 as [???? Step1].
  inversion Step1; unfold isCAS; subst; auto. destruct Hs; by subst.
Qed.

Lemma frame_steps_init s s' T:
  sts.frame_steps T s s' → isInit s → isInit s'.
Proof.
  induction 1 as [|??? Step _ IH]; [done|] => Hs. apply IH. clear IH.
  inversion_clear Step as [??? Step0]. inversion_clear Step0 as [???? Step1].
  inversion Step1; unfold isInit; subst; auto.
Qed.

Lemma frame_steps_RMW_init s s1 s2 T1 T2:
  sts.frame_steps T1 s1 s →
  sts.frame_steps T2 s2 s → isCAS s1 → isInit s2 →
  rtype s = rcIn.
Proof.
  move => Step1 Step2 Eq1 Eq2.
  move : (frame_steps_RMW _ _ _ Step1 Eq1) => Eq3.
  move : (frame_steps_init _ _ _ Step2 Eq2) => [Eq4|//].
  rewrite /isCAS Eq4 in Eq3. destruct Eq3 as [Eq3|Eq3]; by inversion Eq3.
Qed.

Lemma steps_hist_subseteq s1 s2:
  rtc prim_step s1 s2 → rhist s1 ⊆ rhist s2.
Proof.
  induction 1; [done|]. etrans; [|exact IHrtc].
  inversion H => /= //; apply union_subseteq_r.
Qed.

Lemma alloc_local_mono_rsl_state s s' V
  (Safe: alloc_local (rhist s) V)
  (Steps: rtc prim_step s s'):
  alloc_local (rhist s') V.
Proof.
  apply steps_hist_subseteq in Steps.
  destruct Safe as [? [? [? ?]]].
  do 2 eexists. split; [by apply Steps|done].
Qed.

Lemma init_local_mono_rsl_state s s' V
  (Init: init_local (rhist s) V)
  (Steps: rtc prim_step s s'):
  init_local (rhist s') V.
Proof.
  apply steps_hist_subseteq in Steps.
  destruct Init as [? [? [? ?]]].
  do 2 eexists. split; [by apply Steps|done].
Qed.

(** State Updates **)
Definition state_rset_upd s (f: gset gname → gset gname) : state
  := mkRST (rtype s) (f $ rset s) (rhist s).

Definition state_rset_merge i1 i2 i s := state_rset_upd s ([i1,i2] => [i]).
Definition state_rset_split i1 i2 i s := state_rset_upd s ([i] => [i1,i2]).
Definition state_rset_switch i i' s   := state_rset_upd s ([i] => [i']).
Definition state_rset_insert i s      := state_rset_upd s ({[i]} ∪).

Notation "[ i1 , i2 ] ==> [ i ]" := (state_rset_merge i1 i2 i)
  (at level 200, format "[ i1 , i2 ]  ==>  [ i ]") : C_scope.
Notation "[ i ] ==> [ i1 , i2 ]" := (state_rset_split i1 i2 i)
  (at level 200, format "[ i ]  ==>  [ i1 , i2 ]") : C_scope.
Notation "[ i ] ==> [ i' ]" := (state_rset_switch i i')
  (at level 200, format "[ i ]  ==>  [ i' ]") : C_scope.
Notation "[+ i ]" := (state_rset_insert i)
  (at level 200, format "[+ i ]") : C_scope.

Definition state_rhist_upd h s :=
  match s with 
  | mkRST raUn Γ _ => mkRST raIn Γ h
  | mkRST raIn Γ _ => mkRST raIn Γ h
  | mkRST rcUn Γ _ => mkRST rcIn Γ h
  | mkRST rcIn Γ _ => mkRST rcIn Γ h
  end.
Notation "[ h ~>]" := (state_rhist_upd h) (at level 200, format "[ h ~>]") : C_scope.

Lemma state_rhist_upd_hist h s: rhist ([h~>] s) = h.
Proof. by destruct s as [[]]. Qed.

Lemma state_rhist_upd_steps v V s:
  sts.steps (s, ∅) ([{[(VInj v, V)]} ∪ rhist s ~>] s, ∅).
Proof.
  apply rtc_once.
  constructor;
    [destruct s as [[]]; by constructor|abstract set_solver+
    |abstract set_solver+|by destruct s as [[]]].
Qed.

Lemma state_rhist_upd_isInit  h' s : isInit ([h'~>] s).
Proof. by destruct s as [[]]; simpl; [left|left|right|right]. Qed.

(** Properties of merge *)
Lemma state_rset_merge_tokens i1 i2 i Γ:
  i ∉ Γ → i1 ∈ Γ → i2 ∈ Γ →
  !tok Γ ∪ {[Change i1; Change i2]} ≡ !tok ([i1,i2] => [i]) Γ ∪ {[Change i]}.
Proof.
  intros. apply set_unfold_2. split.
  - move => [[i' [? ?]]|[?|?]]; [| left;naive_solver | left;naive_solver].
    case (decide (i' = i)) => [<-|?]; [by right|left; eexists; naive_solver].
  - move => [[i' [? NE]]|?]; [|left; eexists; naive_solver].
    case (decide (i' = i1)) => [<-|?]; [by right; left|].
    case (decide (i' = i2)) => [<-|?]; naive_solver.
Qed.

Lemma state_rset_merge_steps i1 i2 i s
  (In: i ∉ rset s) (In1: i1 ∈ rset s) (In2: i2 ∈ rset s):
  sts.steps (s, {[Change i1; Change i2]}) (([i1,i2] ==> [i]) s, {[Change i]}).
Proof.
  apply rtc_once. constructor.
  - destruct s as [[]]; constructor.
  - abstract set_solver+In1 In2.
  - abstract set_solver+In.
  - by apply state_rset_merge_tokens.
Qed.

Lemma state_rset_merge_included i1 i2 i s: ([i1, i2] ==> [i]) s ∈ i_states i.
Proof.
  apply i_states_in. rewrite /= elem_of_union; left; by apply elem_of_singleton.
Qed.

(** Properties of split **)
Lemma tok_disj_state_singleton s i: i ∈ rset s → sts.tok s ## {[Change i]}.
Proof. abstract set_solver. Qed.

Lemma state_rset_split_token_disj_1 i1 i2 i s:
  sts.tok ([i] ==> [i1,i2] s) ## {[Change i1]}.
Proof. apply tok_disj_state_singleton. abstract set_solver+. Qed.

Lemma state_rset_split_token_disj_2 i1 i2 i s:
  sts.tok ([i] ==> [i1,i2] s) ## {[Change i2]}.
Proof. apply tok_disj_state_singleton. abstract set_solver+. Qed.

Lemma state_rset_split_included_1 i1 i2 i s:
  [i] ==> [i1,i2] s ∈ i_states i1.
Proof. apply i_states_in. abstract set_solver+. Qed.

Lemma state_rset_split_included_2 i1 i2 i s:
  [i] ==> [i1,i2] s  ∈ i_states i2.
Proof. apply i_states_in. abstract set_solver+. Qed.

Lemma state_rset_split_tokens i1 i2 i Γ:
  i ∈ Γ → i1 ∉ Γ → i2 ∉ Γ →
  !tok Γ ∪ {[Change i]} ≡ !tok ([i] => [i1,i2]) Γ ∪ {[Change i1; Change i2]}.
Proof.
  intros. apply set_unfold_2. split.
  - move => [[i' [? ?]]|?]; [|left; eexists; naive_solver].
    case (decide (i' = i1)) => [<-|?]; [by right;left|].
    case (decide (i' = i2)) => [<-|?]; [by right;right|left]. naive_solver.
  - move => [[i' [? ?]]|[?|?]]; [|left; by eexists|left; by eexists].
    case (decide (i' = i)) => [<-|?]; [by right|left]. naive_solver.
Qed.

Lemma state_rset_split_steps i1 i2 i s
  (In: i ∈ rset s) (In1: i1 ∉ rset s) (In2: i2 ∉ rset s):
  sts.steps (s, {[Change i]}) (([i] ==> [i1,i2]) s, {[Change i1; Change i2]}).
Proof.
  apply rtc_once. constructor.
  - destruct s as [[]]; constructor.
  - abstract set_solver+In.
  - abstract set_solver+In1 In2.
  - by apply state_rset_split_tokens.
Qed.

(** Properties of switch **)
Lemma state_rset_switch_tokens i i' Γ
  (Ini : i ∈ Γ) (NIn' : i' ∉ Γ):
  !tok Γ ∪ {[Change i]} ≡ !tok ([i] => [i']) Γ ∪ {[Change i']}.
Proof.
  apply set_unfold_2. split; move => [[i1 [-> NE]]|->].
  - case (decide (i1 = i')) => [->|?]; [by right|left; naive_solver].
  - case (decide (i = i')) => [->|?]; [by right|left; naive_solver].
  - case (decide (i1 = i)) => [->|]; [by right|left; naive_solver].
  - case (decide (i = i')) => [->|?]; [by right|left; naive_solver].
Qed.

Lemma state_rset_switch_steps i i' s (In: i ∈ rset s) (NIn: i' ∉ rset s):
  sts.steps (s, {[Change i]}) (([i] ==> [i']) s, {[Change i']}).
Proof.
  apply rtc_once. constructor.
  - destruct s as [[]]; constructor.
  - by apply tok_disj_state_singleton.
  - apply tok_disj_state_singleton. abstract set_solver+.
  - by apply state_rset_switch_tokens.
Qed.

Lemma state_rset_switch_included i i' s: ([i] ==> [i']) s ∈ i_states i'.
Proof. apply i_states_in. abstract set_solver. Qed.

(** Properties of insert **)
Lemma state_rset_insert_tokens i Γ (NIn : i ∉ Γ):
  !tok Γ ∪ ∅ ≡ !tok ({[i]} ∪ Γ) ∪ {[Change i]}.
Proof.
  rewrite -> right_id; [|eauto with typeclass_instances].
  apply set_unfold_2. split.
  - move => [i' [-> ?]].
    case (decide (i' = i)) => [->|?]; [by right|left; naive_solver].
  - move =>  [[? [-> ?]]|?]; [|by exists i]. naive_solver.
Qed.

Lemma state_rset_insert_steps i s (NI: i ∉ rset s):
  sts.steps (s, ∅) ([+i] s, {[Change i]}).
Proof.
  apply rtc_once. constructor.
  - destruct s as [[]]; constructor.
  - apply disjoint_empty_r.
  - apply tok_disj_state_singleton. abstract set_solver+.
  - by apply state_rset_insert_tokens.
Qed.

Lemma state_rset_insert_included i s: [+i] s ∈ i_states i.
Proof. apply i_states_in. abstract set_solver+. Qed.

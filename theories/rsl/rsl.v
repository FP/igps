From iris.program_logic Require Export weakestpre.
From iris.program_logic Require Import ectx_lifting.
From stdpp Require Import functions.
From iris.base_logic Require Import big_op.
From iris.base_logic.lib Require Import saved_prop sts cancelable_invariants.
From iris.algebra Require Import auth frac sts.
From iris.proofmode Require Import tactics.

Import uPred.
From igps Require Import iris_lemmas.
From igps.lang Require Export lang blocks notation.
From igps.viewpred Require Import viewpred.
From igps.base Require Import na base.
From igps.rsl Require Import rsl_sts vboxes.

Set Default Proof Using "Type".

Lemma sts_auth_frag_in_up `{invG Σ, stsG Σ sts} s T s' T' γ:
  own γ (sts_auth s T) ∗ sts_own γ s' T' ⊢ ⌜s ∈ sts.up s' T'⌝.
Proof.
  rewrite -own_op own_valid. by iDestruct 1 as %?%sts_auth_frag_valid_inv.
Qed.

Implicit Types (Γ: gset gname) (h: History) (l: loc) (V: View)
               (s: state) (q: frac).

Class rslG Σ := RSLG {
  rsl_stsG :> stsG Σ rsl_sts;
  rsl_vboxG :> vboxG Z View Σ;
}.

Definition rslΣ : gFunctors := #[stsΣ rsl_sts; vboxΣ Z View ].

Instance subG_rslΣ {Σ} : subG rslΣ Σ → rslG Σ.
Proof. solve_inG. Qed.

Section proof.
Context `{!foundationG Σ, !rslG Σ}.

Local Notation iProp := (iProp Σ).
Implicit Types (Ψ: gname → Z → View → iProp) (φ: Z → View → iProp).

Section defs.
Local Open Scope I.
Definition conv φ φ' : iProp := □ ∀ v V, φ v V → φ' v V.
Definition slicesA Γ φ Ψ := vbox Γ φ Ψ.
Definition slicesC Γ Ψ := vslices Γ Ψ ∗ ▷ □ ∀ i v V, ⌜i ∈ Γ⌝ → (Ψ i v V ↔ True).
Definition res (φ: Z → View → iProp) (h : History) : iProp :=
  ([∗ set] p ∈ h, match p with
                  | (VInj v,V) => φ v V
                  | (_,_) => True
                  end)%I.

Definition rsl_inv' l φ φd s: iProp :=
  match s with
  | mkRST raUn Γ h =>
      ⌜uninit h⌝ ∗ ∃ Ψ, slicesA Γ φ Ψ    (* Acq slices (splitting) *)
  | mkRST raIn Γ h =>
      ⌜init l h⌝
      ∗ ▷ res φd h              (* duplicated resource *)
      ∗ ∃ Ψ, slicesA Γ φ Ψ    (* Acq slices (splitting) *)
      ∗ ▷ res (λ v V, ([∗ set] i ∈ Γ, Ψ i v V)) h
  | mkRST rcUn Γ h =>
      ⌜uninit h⌝ ∗  ∃ Ψ, slicesC Γ Ψ (* Acq slices (splitting) *)
  | mkRST crcIn Γ h =>
      ⌜init l h⌝
      ∗ ▷ res φd h                  (* duplicated resource *)
      ∗ ▷ res φ (gblock_ends l h)   (* CAS-able resources *)
      ∗ ∃ Ψ, slicesC Γ Ψ          (* Acq slices (splitting) *)
  end.

Definition rsl_inv l φ φd s: iProp := Hist l (rhist s) ∗ rsl_inv' l φ φd s.

Definition RSLInv φ φd l jγ : iProp
  := ∃ (j γ: gname), ⌜jγ = encode (j,γ)⌝ ∗ sts_inv γ (rsl_inv l φ φd).

Definition RelRaw V (φ: Z → vPred) φR jγ : iProp
  := ∃ s (j γ: gname), ⌜jγ = encode (j,γ)⌝
        ∗ sts_own γ s ∅
        ∗ ⌜alloc_local (rhist s) V⌝
        ∗ ▷ conv φ φR ∗ vslice j φR.

Definition AcqRaw V (φ: Z → vPred) jγ : iProp
  := ∃ s (j γ: gname) i Ψi, ⌜jγ = encode (j,γ)⌝
        ∗ sts_own γ s {[ Change i ]}
        ∗ ⌜s ∈ i_states i⌝ ∗ ⌜alloc_local (rhist s) V⌝
        ∗ ▷ conv Ψi φ ∗ vslice i Ψi.

Definition RMWAcqRaw V jγ : iProp
  := ∃ s (j γ: gname), ⌜jγ = encode (j,γ)⌝
        ∗ sts_own γ s ∅
        ∗ ⌜isCAS s⌝ ∗ ⌜alloc_local (rhist s) V⌝.

Definition InitRaw V jγ : iProp
  := ∃ s (j γ: gname), ⌜jγ = encode (j,γ)⌝
        ∗ sts_own γ s ∅
        ∗ ⌜isInit s⌝ ∗ ⌜init_local (rhist s) V⌝.
End defs.

Instance RelRaw_persistent V (φ: Z → vPred) φR jγ
  : Persistent (RelRaw V φ φR jγ) := _.

Instance RMWAcqRaw_persistent V jγ : Persistent (RMWAcqRaw V jγ) := _.

Instance InitRaw_persistent V jγ : Persistent (InitRaw V jγ) := _.

(* Helpers *)
Lemma big_sepS_gblock_ends_ins_update
  l (h: History) p0 (Ψ: Val * View → iProp)
  (Disj: h ## {[p0]})
  (GB: gblock_ends_ins l h p0 (gblock_ends l h) (gblock_ends l ({[p0]} ∪ h)))
  : Ψ p0 ∗ ([∗ set] p ∈ gblock_ends l h, Ψ p)
    ⊢ ([∗ set] p ∈ gblock_ends l ({[p0]} ∪ h), Ψ p).
Proof.
  apply blocks_generic.block_ends_ins_update;
    [by apply disjoint_singleton_r in Disj|auto].
Qed.

(** WRITE *)
Lemma rsl_inv'_write_update l φ φd s v V
  (HTOK: hTime_ok l ({[(VInj v, V)]} ∪ rhist s))
  (INIT: init l ({[(VInj v, V)]} ∪ rhist s)) (NIN: rhist s ## {[VInj v, V]}):
  φ v V -∗ φd v V
  -∗ rsl_inv' l φ φd s
  -∗ rsl_inv' l φ φd ([{[(VInj v, V)]} ∪ rhist s ~>] s).
Proof.
  iIntros "Hφ Hφd".
  have ?: (VInj v, V)  ∉ rhist s by abstract set_solver+NIN.
  destruct s as [[] Γ h]; iIntros "(init & res)"; simpl; iFrame (INIT);
    iDestruct "init" as %OINIT.
  - destruct OINIT. subst h. iSplitL "Hφd".
    { iNext. rewrite /res big_sepS_insert ; last by rewrite elem_of_singleton.
      rewrite big_sepS_singleton. by iFrame "Hφd". }
    iDestruct "res" as (Ψ) "#slices".
    iExists Ψ. iFrame "slices".
    rewrite /res big_sepS_insert; last by rewrite elem_of_singleton.
    rewrite big_sepS_singleton. iDestruct "slices" as "(conv & _)".
    iNext. iSplit; [|done]. by iApply "conv".
  - iDestruct "res" as "[resD res]". iSplitL "Hφd resD".
    { iNext. rewrite /res big_sepS_insert; [iFrame|done]. }
    iDestruct "res" as (Ψ) "[#slices res]".
    iExists Ψ. iFrame "slices". iDestruct "slices" as "(conv & _)".
    iNext. rewrite /res big_sepS_insert; [|done]. iFrame "res". by iApply "conv".
  - destruct OINIT as [V']. subst h. iSplitL "Hφd".
    { iNext. rewrite /res big_sepS_insert ; last by rewrite elem_of_singleton.
      rewrite big_sepS_singleton. by iFrame "Hφd". }
    iDestruct "res" as (Ψ) "#slices". iSplitL "Hφ"; [|by iExists Ψ].
    iNext. iApply (big_sepS_mono _ _ ({[(VInj v, V); (A, V')]}: History));
      [by apply subseteq_gset_filter|reflexivity|].
    rewrite big_sepS_insert; last by rewrite elem_of_singleton.
    rewrite big_sepS_singleton. by iFrame "Hφ".
  - iDestruct "res" as "(resD & res & $)".
    iNext. iSplitL "Hφd resD".
    { rewrite /res big_sepS_insert; [iFrame|done]. }
    iApply (big_sepS_gblock_ends_ins_update); [auto|..|iFrame].
    by apply blocks.gblock_ends_ins_update.
Qed.

Lemma RelRaw_write l (φ φd φR: Z → vPred) v V n E π:
  nclose physN ⊆ E →
    {{{ PSCtx ∗ ▷ Seen π V ∗ ▷ φ v V ∗ ▷ φd v V
          ∗ ▷ RSLInv φR φd l n ∗ RelRaw V φ φR n }}}
      (#l <~ᵃᵗ #v, V)%E @ E
    {{{ V', RET (#(), V') ;
          ⌜V ⊑ V'⌝ ∗ Seen π V'
          ∗ ▷ RSLInv φR φd l n ∗ RelRaw V' φ φR n ∗ InitRaw V' n}}}.
Proof.
  iIntros (??) "(#kI & Seen & Itpr &ItprD & oI & Rel) Post".
  iDestruct "Rel" as (s j γ) "(% & Own & % & #Conv & #Stored)".
  iDestruct "oI" as (j1 γ1) "(>% & Inv)". subst n. apply encode_inj in H2.
  inversion H2. subst j1 γ1.

  iMod (sts_acc with "[$Inv $Own]") as (s') "(Step & (Hist & Inv) & HClose)".
  iDestruct "Step" as %Step%frame_steps_steps.

  assert (alloc_local (rhist s') V). by apply (alloc_local_mono_rsl_state s).

  iApply wp_fupd. iApply (wp_mask_mono (↑physN)); [auto|].
  iApply (f_write_at with "[$kI $Hist $Seen]"); [auto|].

  iNext. iIntros (V' h') "(% & Seen' & Hist & % & % & % & % & _)".
  iMod (Hist_hTime_ok with "[$kI $Hist]") as "(Hist & %)"; first done.

  pose s'' := [h'~>] s'. subst h'.

  iMod ("HClose" $! s'' ∅ with "[Itpr ItprD Inv Hist]") as "(Inv & #Own)".
  { iSplitL "".
    - iPureIntro. by apply (state_rhist_upd_steps v V').
    - rewrite /rsl_inv. iNext. rewrite state_rhist_upd_hist. iFrame "Hist".
      iDestruct ("Conv" with "Itpr") as "Itpr".
      iApply (rsl_inv'_write_update with "[Itpr] [ItprD] Inv");
        [done|done|done|..]; by iApply (vPred_mono V). }

  iModIntro. iApply ("Post" $! V'). iFrame (H3) "Seen'".
  iSplitL "Inv". { iExists j, γ. by iFrame "Inv". }

  have ?: init_local (rhist s'') V' by rewrite state_rhist_upd_hist.
  iSplit; iExists s'', j, γ; iFrame "Own"; (iSplit; first auto).
  - iFrame "Conv Stored". iPureIntro. by apply alloc_init_local.
  - iPureIntro. split; [|done]. by apply state_rhist_upd_isInit.
Qed.

(** READ *)
Lemma rsl_inv'_read_update l φ φR φd i Ψi s v V
  (INIT: isInit s) (IN: (VInj v, V) ∈ rhist s) (INi: i ∈ rset s) :
  (conv Ψi φ -∗ vslice i Ψi
  -∗ rsl_inv' l φR φd s
  -∗ ▷ (φd v V -∗ φd v V ∗ φd v V)
  ==∗ ∃ i', ⌜i' ∉ rset s⌝ ∗ vslice i' (<[v:= (λ _, True)]> Ψi)
    ∗ rsl_inv' l φR φd ([i] ==> [i'] s)
    ∗ ▷ φ v V ∗ ▷ φd v V)%I.
Proof.
  iIntros "conv slice Inv Dup".
  destruct s as [[] Γ h];
    [by destruct INIT|clear INIT|by destruct INIT|clear INIT];
  simpl in *; iDestruct "Inv" as "($ & resD & res)".
  - (* get dup interp *)
    iDestruct (big_sepS_dup_later _ _ _ IN with "[Dup $resD]")
      as "($ & $)"; [done|].
    (* get full interp *)
    iDestruct "res" as (Ψ) "[#slices res]".
    rewrite /res (big_sepS_delete _ _ _ IN) (big_sepS_delete _ _ _ INi).
    iDestruct "res" as "((Hφ & res1) & res2)".
    iDestruct (vbox_agree with "slice slices") as "#Eq"; [done|].
    rewrite vpred_equiv_unfold.
    iMod (vslice_switch_drop_value Γ i _ _ _ v false with "slice slices")
      as (i') "(Hi' & #slice' & #slices')"; [exact INi|].
    iExists i'. iDestruct "Hi'" as %NIn'. iFrame (NIn') "slice'".
    iModIntro. iSplitR "Hφ conv".
    + iExists (<[i':=<[v:=λ _, True%I]> Ψi]> Ψ). iFrame "slices'".
      iNext. rewrite (big_sepS_delete _ _ _ IN).
      iSplitL "res1".
      * rewrite (big_sepS_fn_insert (λ _ (f: Z → View → iProp), f v V));
          [|abstract set_solver+NIn'].
        rewrite fn_lookup_insert. by iFrame "res1".
      * iApply (big_sepS_impl _ _ (h ∖ {[VInj v, V]}) with "[$res2]").
        iIntros "!#" ([[| |v2] V2] InvV) "Hs"; [done|done|].
        rewrite (big_sepS_fn_insert (λ _ (f: Z → View → iProp), f v2 V2));
          [|abstract set_solver+NIn'].
        case (decide (v2 = v)) => [->|?];
          [rewrite fn_lookup_insert|rewrite fn_lookup_insert_ne; last auto].
        { iSplit; first done. rewrite (big_sepS_delete _ _ _ INi).
          iDestruct "Hs" as "[_ $]". }
        { iRewrite ("Eq" $! v2 V2). by rewrite -big_sepS_delete. }
    + iNext. iApply "conv". by iRewrite ("Eq" $! v V).
  - (* get dup interp *)
    iDestruct (big_sepS_dup_later _ _ _ IN with "[Dup $resD]")
      as "($ & $)"; [done|].
    iDestruct "res" as "[$ #slices]".
    iDestruct "slices" as (Ψ) "[slices EqT]".
    iDestruct (vslices_agree with "slice slices") as "#Eq"; [done|].
    rewrite vpred_equiv_unfold.
    iMod (vslice_switch_drop_value Γ i _ φR Ψ v false with "slice [slices]")
      as (i') "(Hi' & #slice' & #slices')"; [exact INi|..].
    + unfold vbox. iFrame "slices". iNext. iNext.
      iIntros "!#" (v2 V2) "_".
      iApply (big_sepS_impl (λ _, True%I) _ Γ with "[]"). iSplit.
      * iIntros "!#" (i0) "IN _". by iApply ("EqT" $! i0 v2 V2 with "IN").
      * by rewrite big_sepS_forall.
    + iExists i'. iDestruct "Hi'" as %NIn'. iFrame (NIn') "slice'".
      iModIntro. iSplitR "conv".
      * iExists _. iDestruct "slices'" as "[? $]".
        iNext. iIntros "!#" (i0 v0 V0 In0).
        case (decide (i0 = i')) => [->|Neq'];
          [rewrite fn_lookup_insert|rewrite fn_lookup_insert_ne; last done].
        { case (decide (v0 = v)) => [->|?];
          [by rewrite fn_lookup_insert|rewrite fn_lookup_insert_ne; last done].
          iRewrite ("Eq" $! v0 V0). by iApply ("EqT" $! i v0 V0 with "[%]"). }
        { iApply ("EqT" $! i0 v0 V0 with "[%]"). abstract set_solver+In0 Neq'. }
      * iNext. iApply "conv". iRewrite ("Eq" $! v V).
        by iApply ("EqT" $! i v V with "[%]").
Qed.

(* TODO: further refactor this into view shifts *)
Lemma AcqRaw_read l (φ φd φR: Z → vPred) V n E π:
  nclose physN ⊆ E →
    {{{ inv physN PSInv ∗ ▷ Seen π V
        ∗ (∀ V v, φd v V -∗ φd v V ∗ φd v V)
        ∗ ▷ RSLInv φR φd l n ∗ AcqRaw V φ n ∗ InitRaw V n }}}
      (!ᵃᵗ #l, V) @ E
    {{{ (v:Z) V', RET (#v, V');
        ⌜V ⊑ V'⌝ ∗ Seen π V'
        ∗ ▷ RSLInv φR φd l n
        ∗ AcqRaw V' (<[v:=True%VP]>φ) n ∗ InitRaw V' n
        ∗ ▷ φ v V' ∗ ▷ φd v V'}}}.
Proof.
  iIntros (? Φ) "(#kI & kS & Dup & oI & AR & IR) Post".
  iDestruct "AR" as (s1 j γ i Ψi) "(% & Own1 & Hs1 & Alloc & #Conv & #SΨi)".
  iDestruct "Hs1"  as %Hs1.
  iDestruct "Alloc" as %Alloc1.
  iDestruct "IR" as (s2 j1 γ1) "(% & #Own2 & IR)".
  subst n. apply encode_inj in H1. inversion H1. subst j1 γ1.
  iDestruct "IR" as %[Inits2 Init2].
  rewrite {1}/RSLInv /sts_inv.
  iDestruct "oI" as (j2 γ2) "(>% & Inv)".
  apply encode_inj in H0. inversion H0. subst j2 γ2.

  iDestruct "Inv" as (s) "(>Own & Hist & Inv)".
  iDestruct (sts_auth_frag_in_up with "[$Own $Own1]") as %Up1.
  iDestruct (sts_auth_frag_in_up with "[$Own $Own2]") as %Up2.

  assert (HInit: init_local (rhist s) V).
  { apply (init_local_mono_rsl_state s2 _ _ Init2).
    by apply (frame_steps_steps _ _ ∅). }
  assert (HAlloc := alloc_init_local _ _ HInit).
  assert (HSub := sts.up_subseteq s1 _ _ (i_states_closed i) Hs1).
  have Inits: isInit s by apply (frame_steps_init s2 s ∅).

  iApply (wp_mask_mono (↑physN)); [auto|]. iApply wp_fupd.
  iApply (f_read_at with "[$Hist $kS $kI]"); [auto|].
  iNext. iIntros (v V') "(% & kS' & Hist & % & vV)".
  iDestruct "vV" as (V1) "(% & % & %)".

  have Ini : i ∈ rset s by apply HSub in Up1.

  iMod (rsl_inv'_read_update l _ _ _ i _ s v V1 Inits with "Conv SΨi Inv [Dup]")
    as (i') "(% & slice' & Inv' & Hφ & Hφd)"; [done|done|..].
  { iNext. by iApply "Dup". }

  pose s' := [i] ==> [i'] s.

  rewrite /sts_own. iCombine "Own" "Own1" as "Own".
  rewrite sts_op_auth_frag;
    [|auto|by apply sts.closed_up, tok_disj_state_singleton].
  iMod (own_update _ _ (sts_auth s' {[Change i']}) with "[$Own]") as "Own".
  { by apply sts_update_auth, state_rset_switch_steps. }

  rewrite -2!sts_op_auth_frag_up. iDestruct "Own" as "[[Own #Own1] Own']".

  iMod (Hist_hTime_ok with "[$Hist $kI]") as "(Hist & %)"; [auto|].
  iApply ("Post" $! v V').
  iModIntro. iSplit; [done|]. iFrame "kS'".
  iDestruct (vPred_mono _ V' with "Hφ") as "$"; [done|].
  iDestruct (vPred_mono _ V' with "Hφd") as "$"; [done|].
  iSplitL "Inv' Own Hist"; last iSplitR "".
  - iNext. iExists j, γ. iSplitL ""; first auto.
    rewrite /sts_inv. iExists s'. iFrame "Own Hist Inv'".
  - iExists s', j, γ, i', _. rewrite /sts_own. iFrame "Own' slice'".
    repeat iSplit; [auto|..].
    + iPureIntro. by apply state_rset_switch_included.
    + iPureIntro. by apply (alloc_local_Mono _ _ V).
    + iNext. iIntros "!#" (v2 V2).
      case (decide (v2 = v)) => [->|?].
      * rewrite 2!fn_lookup_insert. done.
      * do 2 (rewrite fn_lookup_insert_ne; last done). iApply "Conv".
  - iExists s',j,γ. rewrite /sts_own. iFrame "Own1".
    repeat iSplit; [auto|..]; iPureIntro;
      [done|by apply (init_local_Mono _ _ V)].
Qed.

(** RMWAcq SPLIT *)
Lemma rsl_slicesC_insert Γ (q: bool) :
  ▷?q (∃ Ψ, slicesC Γ Ψ) ==∗
  ∃ i, ⌜i ∉ Γ⌝ ∗ vslice i (λ _ _, True) ∗ ▷?q ∃ Ψ, slicesC ({[i]} ∪ Γ) Ψ.
Proof.
  iDestruct 1 as (Ψ) "(slices & #True)".
  iMod (vslices_insert2 _ Γ _  (λ _ _, True%I) q with "slices")
    as (i) "(% & slice & slices)"; [done|].
  iExists i. iIntros "!> {$%}".
  iFrame "slice". iExists (<[i:= (λ _ _, True%I)]> Ψ). iFrame "slices".
  do 2 iNext. iIntros "!#" (??).
  iDestruct 1 as %[->%elem_of_singleton_1|?]%elem_of_union.
  - by rewrite fn_lookup_insert.
  - rewrite fn_lookup_insert_ne; [by iApply "True"|intros ?; by subst].
Qed.

Lemma rsl_inv_insertC s l φ φd (q: bool) (ISCAS: isCAS s) :
  (▷?q rsl_inv l φ φd s)
  ==∗ ∃ i, ⌜i ∉ rset s⌝ ∗ vslice i (λ _ _, True) ∗ ▷?q rsl_inv l φ φd ([+i] s).
Proof.
  iIntros "[hist Inv]". move : ISCAS. unfold isCAS.
  destruct s as [[]]; simpl;
    [by destruct 1|by destruct 1|move => [_|//]|move => [//|_]]; iFrame "hist";
    [iDestruct "Inv" as "($ & sC)"|
      iDestruct "Inv" as "($ & $ & $ & sC)"];
  by iApply (rsl_slicesC_insert with "sC").
Qed.

Lemma conv_id φ : conv φ φ.
Proof. by iIntros "!#" (??) "$". Qed.

Lemma RMWAcqRaw_split l (φ φd: Z → vPred) V n E:
  ▷ RSLInv φ φd l n ∗ RMWAcqRaw V n
  ⊢ |={E}=> ▷ RSLInv φ φd l n ∗ RMWAcqRaw V n ∗ AcqRaw V (λ _, True)%VP n.
Proof.
  iIntros "(Inv & #RR)".
  iDestruct "RR" as (s j γ) "(% & #Own & RR)".
  iDestruct "RR" as "(% & %)".
  iDestruct "Inv" as (j1 γ1) "(>% & Inv)". subst n.
  apply encode_inj in H2. inversion H2. subst j1 γ1.

  iMod (sts_acc with "[$Inv $Own]") as (s') "(% & Inv & SClose)".
  iMod (rsl_inv_insertC s' l φ φd true with "Inv") as (i) "(% & #Hi & Inv)";
    [by eapply frame_steps_RMW|].

  iMod ("SClose" $! ([+i] s') {[Change i]} with "[$Inv]") as "(Inv & Own')";
    [iPureIntro; by apply state_rset_insert_steps|].

  iModIntro.
  iSplitL "Inv"; last iSplitR "Own'".
  - iNext. iExists j,γ. by iFrame.
  - iExists s,j,γ. by iFrame "Own".
  - iExists _, j,γ, i, (λ _ : Z, True%VP). iFrame "Own' Hi".
    repeat iSplit; first auto.
    + iPureIntro. by apply state_rset_insert_included.
    + iPureIntro. apply (alloc_local_mono_rsl_state s s'); [done|].
      by apply (frame_steps_steps _ _ ∅).
    + iNext. iApply conv_id.
Qed.

(** MERGE *)
Lemma RelRaw_split (φ1 φ2 φR: Z → vPred) n V:
  RelRaw V (λ v, φ1 v ∨ φ2 v)%VP φR n -∗ RelRaw V φ1 φR n ∗ RelRaw V φ2 φR n.
Proof.
  iDestruct 1 as (s j γ) "(F1 & oF & F2 & #conv & slice)".
  iSplit; iExists s,j,γ; iFrame "F1 oF F2 slice";
    iNext; iIntros "!#" (? ?) "?"; iApply "conv"; [by iLeft|by iRight].
Qed.

Lemma RelRaw_merge (φ1 φ2 φR1 φR2: Z → vPred) n V:
  RelRaw V φ1 φR1 n -∗ RelRaw V φ2 φR2 n
  -∗ RelRaw V (λ v, φ1 v ∨ φ2 v)%VP φR1 n.
Proof.
  iDestruct 1 as (s1 j1 γ1) "(% & #Rel1 & % & #Conv1 & #SP1)".
  iDestruct 1 as (s2 j2 γ2) "(% & #Rel2 & % & #Conv2 & #SP2)".
  subst n. apply encode_inj in H1. inversion H1. subst.
  iDestruct (vslice_agree with "SP1 SP2") as "#Eq".
  rewrite vpred_equiv_unfold.
  iExists s1,j2,γ2. iFrame (H0) "Rel1 SP1". iSplit; first auto.
  iNext. iIntros "!#" (v' V') "[?|?]".
  - by iApply "Conv1".
  - iRewrite ("Eq" $! v' V'). by iApply "Conv2".
Qed.

Lemma rsl_slices_merge_CAS Γ i1 i2 Ψi1 Ψi2 (q: bool)
  (In1: i1 ∈ Γ) (In2: i2 ∈ Γ) (NEq: i1 ≠ i2):
  vslice i1 Ψi1 -∗ vslice i2 Ψi2 -∗ ▷?q (∃ Ψ , slicesC Γ Ψ)
  ==∗ ∃ i, ⌜i ∉ Γ⌝ ∗ vslice i (λ v V, Ψi1 v V ∗ Ψi2 v V)
           ∗ ▷?q ∃ Ψ, slicesC (([i1,i2] => [i]) Γ) Ψ.
Proof.
  iIntros "slice1 slice2". iDestruct 1 as (Ψ) "[slices #True]".
  have LE : Γ ∖ {[i1; i2]} ⊆ Γ by abstract set_solver+.
  iDestruct (vslices_agree with "slice1 slices") as "#Eq1"; [done|].
  iDestruct (vslices_agree with "slice2 slices") as "#Eq2"; [done|].
  iDestruct (vslices_mono _ (Γ ∖ {[i1; i2]}) with "slices") as "slices";
    [exact LE|].
  iMod (vslices_insert2 _ Γ _ (λ v V, Ψi1 v V ∗ Ψi2 v V)%I q LE with "slices")
    as (i) "(#Hi & slice & slices)".
  iModIntro. iExists (i). iFrame "Hi slice".
  iExists _. iFrame "slices".
  do 2 iNext. iIntros "!#" (i' v' V' In').
  case (decide (i' = i)) => [->|NEq'].
  - rewrite fn_lookup_insert.
    rewrite 2!vpred_equiv_unfold.
    iRewrite ("Eq1" $! v' V'). iRewrite ("Eq2" $! v' V').
    iSplit; iIntros "_"; [done|]. iSplitL.
    + by iApply ("True" $! i1 v' V' with "[]").
    + by iApply ("True" $! i2 v' V' with "[]").
  - rewrite fn_lookup_insert_ne; [|done].
    iDestruct "Hi" as %NIni. iApply "True".
    iPureIntro. abstract set_solver+In' NEq'.
Qed.

Lemma rsl_inv_merge l φR φd s i1 i2 Ψi1 Ψi2 (q: bool)
  (In1: i1 ∈ rset s) (In2: i2 ∈ rset s) (NEq: i1 ≠ i2):
  vslice i1 Ψi1 -∗
  vslice i2 Ψi2 -∗
  ▷?q rsl_inv l φR φd s ==∗
    ∃ i, ⌜i ∉ rset s⌝ ∗ vslice i (λ v V, Ψi1 v V ∗ Ψi2 v V)
          ∗ ▷?q rsl_inv l φR φd ([i1,i2]==> [i] s).
Proof.
  iIntros "slice1 slice2". iDestruct 1 as "[Hist Inv]".
  destruct s as [[] Γ h]; iFrame "Hist"; simpl in *;
    iDestruct "Inv" as "[$ Inv]".
  - iDestruct "Inv" as (Ψ) "slices".
    iMod (vslice_merge _ _ _ _ (λ v V, Ψi1 v V ∗ Ψi2 v V)%I _ _ _ q
      with "slice1 slice2 [] slices") as (i) "(#Hi & slice & slices)";
      [done|done|done|..].
    { do 2 iNext. iIntros "!#" (??) "[$$]". }
    iModIntro. iExists (i). iFrame "Hi slice". iExists _. iFrame "slices".
  - iDestruct "Inv" as "[$ Inv]". iDestruct "Inv" as (Ψ) "[slices res]".
    iDestruct (vbox_agree with "slice1 slices") as "#Eq1"; [done|].
    iDestruct (vbox_agree with "slice2 slices") as "#Eq2"; [done|].
    iMod (vslice_merge _ _ _ _ (λ v V, Ψi1 v V ∗ Ψi2 v V)%I _ _ _ q
      with "slice1 slice2 [] slices") as (i) "(#Hi & slice & slices)";
      [done|done|done|..].
    { do 2 iNext. iIntros "!#" (??) "[$$]". }
    iModIntro. iExists (i). iFrame "Hi slice". iExists _. iFrame "slices".
    do 2 iNext.
    iApply (big_sepS_impl _ _ h with "[$res]").
    iIntros "!#" ([[| |v'] V'] In') "res"; [done|done|].
    iDestruct "Hi" as %NIni.
    rewrite big_sepS_insert; [|abstract set_solver+NIni].
    (* FIXME: make Leibniz version *)
    have EQΓ: Γ ∖ {[i1; i2]} = Γ ∖ {[i1]} ∖ {[i2]}.
    { apply leibniz_equiv. by rewrite -difference_twice_union. }
    rewrite EQΓ. rewrite (big_sepS_delete _ Γ i1); [|done].
    rewrite (big_sepS_delete _ (Γ ∖ {[i1]}) i2); [|abstract set_solver+In2 NEq].
    iDestruct "res" as "(res1 & res2 & res)".
    iSplitR "res".
    + rewrite fn_lookup_insert 2!vpred_equiv_unfold.
      iRewrite ("Eq1" $! v' V'). iRewrite ("Eq2" $! v' V'). by iFrame.
    + iApply (big_sepS_mono _ _ (Γ ∖ {[i1]} ∖ {[i2]}) with "res"); [done|].
      iIntros (i' Ini') "resi".
      rewrite fn_lookup_insert_ne; [done|].
      intros ?. subst i'. abstract set_solver+NIni Ini'.
  - by iApply (rsl_slices_merge_CAS with "slice1 slice2 Inv").
  - iDestruct "Inv" as "($ & $ & Inv)".
    by iApply (rsl_slices_merge_CAS with "slice1 slice2 Inv").
Qed.

Lemma AcqRaw_merge l (φ1 φ2 φR φd: Z → vPred) n V E:
  ▷ RSLInv φR φd l n -∗ AcqRaw V φ1 n -∗ AcqRaw V φ2 n
  ={E}=∗ ▷ RSLInv φR φd l n ∗ AcqRaw V (λ v, φ1 v ∗ φ2 v)%VP n.
Proof.
  iIntros "Inv".
  iDestruct 1 as (s1 j γ i1 Ψi1) "(% & Own1 & % & % & #Conv1 & #SP1)".
  iDestruct 1 as (s2 j2 γ2 i2 Ψi2) "(% & Own2 & % & % & #Conv2 & #SP2)".
  subst n. apply encode_inj in H2. inversion H2. subst j2 γ2.
  iDestruct "Inv" as (j3 γ3) "(>% & Inv)". rewrite /sts_inv /sts_own.
  iDestruct "Inv" as (s) "(>OwnA & Inv)".
  apply encode_inj in H. inversion H. subst j3 γ3.
  iDestruct (own_valid with "Own1") as %[C1 _]%sts_frag_valid.
  iDestruct (own_valid with "Own2") as %[C2 _]%sts_frag_valid.
  iCombine "Own1" "Own2" as "Own".
  iDestruct (own_valid with "Own") as %C3.
  destruct C3 as [_ [_ C3]]. inversion C3 as [? ? ? ? ? C4 | |].
  rewrite -sts_op_frag; [|auto|auto|auto].

  iCombine "OwnA" "Own" as "OwnA".
  iDestruct (own_valid with "OwnA") 
    as %[Hs1 Hs2]%sts_auth_frag_valid_inv%elem_of_intersection.
  assert (Hi1: s ∈ i_states i1).
    by apply (sts.up_subseteq s1 _ _ (i_states_closed i1)).
  assert (Hi2: s ∈ i_states i2).
    by apply (sts.up_subseteq s2 _ _ (i_states_closed i2)).
  rewrite sts_op_auth_frag; [|done|by apply sts.closed_op].
  assert (i1 ≠ i2). { move => ?. subst. abstract set_solver+C4. }

  iMod (rsl_inv_merge _ _ _ _ i1 i2 _ _ true with "SP1 SP2 Inv")
    as (i) "(% & slice & Inv)"; [done|done|done|].

  pose s' := ([i1,i2] ==> [i]) s.

  assert (ALLOC: alloc_local (rhist s') V).
  { apply (alloc_local_mono_rsl_state s2 s); [done|].
    by apply (frame_steps_steps _ _ {[Change i2]}). }

  iMod (own_update _ _ (sts_auth s' {[Change i]}) with "OwnA") as "OwnA".
  { by apply sts_update_auth, state_rset_merge_steps. }

  rewrite -sts_op_auth_frag_up. iDestruct "OwnA" as "(OwnA & Owni)".
  iModIntro. iSplitR "Owni slice".
  - iNext. iExists j,γ. iSplit; first auto. unfold sts_inv. iExists s'.
    iFrame "OwnA Inv".
  - iExists s',j,γ, i, _. rewrite /sts_own.
    iFrame (ALLOC) "Owni slice". repeat iSplit; first auto.
    + iPureIntro. by apply state_rset_merge_included.
    + iNext. iIntros "!#". iIntros (? ?) "(HΨ1 & HΨ2)".
      iSplitL "HΨ1"; [by iApply "Conv1"|by iApply "Conv2"].
Qed.

Lemma rsl_slices_split_CAS Γ i Ψi φ1 φ2 (q: bool) (Ini: i ∈ Γ) :
  ▷?q □ (∀ v V, Ψi v V -∗ φ1 v V ∗ φ2 v V) -∗
  vslice i Ψi -∗
  ▷?q (∃ Ψ , slicesC Γ Ψ)
  ==∗ ∃ i1 i2, ⌜i1 ∉ Γ ∧ i2 ∉ Γ ∧ i1 ≠ i2⌝
      ∗ vslice i1 φ1 ∗ vslice i2 φ2
      ∗ ▷?q ∃ Ψ, slicesC (([i] => [i1,i2]) Γ) Ψ.
Proof.
  iIntros "#conv slice". iDestruct 1 as (Ψ) "[slices #True]".
  iDestruct (vslices_agree with "slice slices") as "#Eq"; [done|].
  have LE : Γ ∖ {[i]} ⊆ Γ by abstract set_solver+.
  iDestruct (vslices_mono _ (Γ ∖ {[i]}) with "slices") as "slices";
    [exact LE|].
  iMod (vslices_insert2 _ Γ _ φ1 q LE with "slices")
    as (i1) "(Hi1 & slice1 & slices)".
  iMod (vslices_insert2 _ ({[i1]} ∪ Γ) _ φ2 q with "slices")
    as (i2) "(Hi2 & slice2 & slices)"; [by apply union_mono_l|].
  iModIntro. iExists i1, i2. iFrame "slice1 slice2".
  iDestruct "Hi1" as %NIn1. iDestruct "Hi2" as %NIn2.
  iSplit.
  { iPureIntro. repeat split; [done|..]; abstract set_solver+NIn2. }
  rewrite (_: {[i2]} ∪ ({[i1]} ∪ Γ ∖ {[i]}) = ([i] => [i1,i2]) Γ);
    last by rewrite union_comm_L -union_assoc_L
            (union_comm_L _ {[i2]}) union_assoc_L.
  iExists _. iFrame "slices".
  do 2 iNext. iIntros "!#" (i' v' V' In'). rewrite vpred_equiv_unfold.
  iDestruct ("conv" $! v' V' with "[]") as "[Hφ1 Hφ2]".
  { iRewrite ("Eq" $! v' V').
    by iApply ("True" $! i v' V' with "[%]"). }
  case (decide (i' = i2)) => [?|NEq2].
  { subst; rewrite fn_lookup_insert. iSplit; by iIntros "_". }
  rewrite fn_lookup_insert_ne; [|done].
  case (decide (i' = i1)) => [?|NEq1].
  { subst; rewrite fn_lookup_insert. iSplit; by iIntros "_". }
  rewrite fn_lookup_insert_ne; [|done].
  iApply ("True" $! i' v' V' with "[]"). iPureIntro.
  abstract set_solver+In' NEq2 NEq1.
Qed.

Lemma rsl_inv_split l φR φd s i Ψi φ1 φ2 (q: bool) (Ini: i ∈ rset s) :
  ▷?q □ (∀ v V, Ψi v V -∗ φ1 v V ∗ φ2 v V) -∗
  vslice i Ψi -∗ ▷?q rsl_inv l φR φd s
  ==∗ ∃ i1 i2, ⌜i1 ∉ rset s ∧ i2 ∉ rset s ∧ i1 ≠ i2⌝
      ∗ vslice i1 φ1 ∗ vslice i2 φ2
      ∗ ▷?q rsl_inv l φR φd ([i]==> [i1,i2] s).
Proof.
  iIntros "#conv #slice". iDestruct 1 as "[Hist Inv]".
  destruct s as [[] Γ h]; iFrame "Hist"; simpl in *;
    iDestruct "Inv" as "[$ Inv]".
  - iDestruct "Inv" as (Ψ) "slices".
    iMod (vslice_split Γ i _ _ _ _ _ q with "slice [] slices")
      as (i1 i2) "(#Hi & slice1 & slice2 & slices)"; [done|by iNext|].
    iModIntro. iExists i1, i2. iFrame "Hi slice1 slice2".
    iNext. iExists _. iFrame "slices".
  - iDestruct "Inv" as "[$ Inv]". iDestruct "Inv" as (Ψ) "[#slices res]".
    iMod (vslice_split Γ i _ _ _ _ _ q with "slice [] slices")
      as (i1 i2) "(#Hi & slice1 & slice2 & slices')"; [done|by iNext|].
    iModIntro. iExists i1, i2. iFrame "Hi slice1 slice2".
    iNext. iExists _. iFrame "slices'".
    iDestruct (vbox_agree with "slice slices") as "#Eq"; [done|].
    iNext. iApply (big_sepS_impl _ _ h with "[$res]").
    iIntros "!#" ([[| |v'] V'] In') "res"; [done|done|].
    iDestruct "Hi" as %(NIn1 & NIn2 & NEq).
    rewrite big_sepS_union; [|abstract set_solver+NIn1 NIn2].
    rewrite big_sepS_insert; [|abstract set_solver+NEq].
    rewrite big_sepS_singleton fn_lookup_insert fn_lookup_insert_ne; [|done].
    rewrite fn_lookup_insert (big_sepS_delete _ _ _ Ini).
    rewrite vpred_equiv_unfold.
    iDestruct "res" as "[HΨ res]".
    iSplitL "HΨ".
    + iApply "conv". by iRewrite ("Eq" $! v' V').
    + iApply (big_sepS_mono _ _ (Γ ∖ {[i]}) with "res"); [done|].
      iIntros (i' Ini') "HΨ".
      rewrite fn_lookup_insert_ne; [|abstract set_solver+Ini' NIn1].
      rewrite fn_lookup_insert_ne; [done|abstract set_solver+Ini' NIn2].
  - by iApply (rsl_slices_split_CAS with "conv slice Inv").
  - iDestruct "Inv" as "($ & $ & Inv)".
    by iApply (rsl_slices_split_CAS with "conv slice Inv").
Qed.

Lemma AcqRaw_split l (φ1 φ2 φR φd: Z → vPred) V n E:
  ▷ RSLInv φR φd l n -∗ AcqRaw V (λ v : Z, (φ1 v ∗ φ2 v)%VP) n
  ={E}=∗ ▷ RSLInv φR φd l n ∗ AcqRaw V φ1 n ∗ AcqRaw V φ2 n.
Proof.
  iIntros "Inv AC".
  iDestruct "AC" as (s j γ i Ψi) "(% & Own & % & % & #conv & #slice)".
  iDestruct "Inv" as (j1 γ1) "(>% & Inv)". subst n.
  apply encode_inj in H2. inversion H2. subst j1 γ1.
  iMod (sts_acc with "[$Inv $Own]") as (s') "(Step & Inv & HClose2)".
  iDestruct "Step" as %Step.
  assert (s' ∈ i_states i).
  { move: Step. apply sts.closed_steps; [apply i_states_closed|auto]. }

  iMod (rsl_inv_split _ _ _ _ _ _ φ1 φ2 true  with "[conv] slice Inv")
    as (i1 i2) "(Hi12 & #slice1 & #slice2 & Inv)"; [done|..].
  { iNext. iIntros "!#" (v' V') "Hφ". by iApply ("conv" with "Hφ"). }
  iDestruct "Hi12" as %(NIn1 & NIn2 & NEq).

  pose s'' := [i] ==> [i1,i2] s'.

  assert (ALLOC: alloc_local (rhist s'') V).
  { apply (alloc_local_mono_rsl_state s s'); [done|].
    by eapply frame_steps_steps. }

  iMod ("HClose2" $! s'' {[Change i1; Change i2]} with "[$Inv]")
    as "(Inv & Own)"; [iPureIntro; by apply state_rset_split_steps|].

  assert (sts.closed (sts.up s'' {[Change i1]}) {[Change i1]}).
  { apply sts.closed_up, state_rset_split_token_disj_1. }

  assert (sts.closed (sts.up s'' {[Change i2]}) {[Change i2]}).
  { apply sts.closed_up, state_rset_split_token_disj_2. }

  iAssert (|==> sts_own γ s'' {[Change i1]} ∗ sts_own γ s'' {[Change i2]})%I
    with "[Own]" as ">(O1 & O2)".
  { rewrite -sts_ownS_op; [..|auto|auto].
    - iApply (sts_own_weaken with "Own"); [auto| |by apply sts.closed_op].
      apply elem_of_intersection. split; apply sts.elem_of_up.
    - apply disjoint_singleton_l.
      move => /elem_of_singleton. inversion 1. by subst. }

  iModIntro. iSplitL "Inv"; last iSplitL "O1".
  - iNext. iExists j,γ. by iFrame.
  - iExists _, j, γ, i1, φ1. iFrame (ALLOC) "O1 slice1".
    repeat iSplit; [done|..].
    + iPureIntro. apply state_rset_split_included_1.
    + iNext. iIntros "!#" (??). auto.
  - iExists _, j, γ, i2, φ2. iFrame (ALLOC) "O2 slice2".
    repeat iSplit; [done|..].
    + iPureIntro. apply state_rset_split_included_2.
    + iNext. iIntros "!#" (? ?). auto.
Qed.

(** AcqRaw ALLOC *)
Lemma AcqRaw_alloc_vs l h (φ φd: Z → vPred) V:
  Hist l h -∗ ⌜alloc_local h V⌝ -∗ ⌜uninit h⌝
  ==∗ ∃ n, ▷ RSLInv φ φd l n ∗ RelRaw V φ φ n ∗ AcqRaw V φ n.
Proof.
  iIntros "Hist #ALLOC #UNI".
  iMod (vbox_alloc_singleton φ) as (i) "[#slice slices]".

  set s: state := (mkRST raUn {[i]} h).
  iAssert (rsl_inv l φ φd s) with "[Hist slices]" as "Inv".
  { iFrame "Hist UNI". iExists _. iFrame "slices". }

  iMod (own_alloc (sts_auth s {[Change i]})) as (γ) "Own".
  { apply sts_auth_valid, tok_disj_state_singleton.
    simpl; by apply elem_of_singleton. }
  rewrite -2!sts_op_auth_frag_up.
  iDestruct "Own" as "[[OwnA #Own1] Own2]".
  iExists (encode (i, γ)). iModIntro.
  iSplitL "Inv OwnA"; last iSplitR "Own2".
  - iExists _,_. iSplit; first auto. rewrite /sts_inv. iExists _. by iFrame. 
  - iExists _,i,γ. rewrite /sts_own. iFrame "slice Own1 ALLOC".
    iSplit; first auto. iNext. iApply conv_id.
  - iExists _, i, γ, i, _. rewrite /sts_own. iFrame "slice Own2".
    repeat iSplit; [auto| |auto|].
    + iPureIntro. apply i_states_in. simpl. by apply elem_of_singleton.
    + iNext. iApply conv_id.
Qed.

Lemma AcqRaw_alloc (φ φd: Z → vPred) E π V:
  nclose physN ⊆ E →
  {{{ inv physN PSInv ∗ ▷ Seen π V }}}
    (Alloc, V) @ E
  {{{ (l: loc) V' n X, RET (#l, V');
        ⌜V ⊑ V'⌝ ∗ Seen π V' ∗ Info l 1 X
      ∗ ▷ RSLInv φ φd l n ∗ RelRaw V' φ φ n ∗ AcqRaw V' φ n }}}.
Proof.
  iIntros (? Φ) "(kI & kS) Post".
  iApply (wp_mask_mono (↑physN)); first auto. iApply wp_fupd.
  iApply (f_alloc with "[$kI $kS]"). iNext.
  iIntros (l V' h X) "(VV' & kS' & Hist & Info & _ & Alloc & Uninit)".

  iMod (AcqRaw_alloc_vs l h φ φd V' with "Hist Alloc Uninit")
    as (n) "IRA".
  iApply ("Post" $! l V' n X with "[$VV' $kS' $Info $IRA]").
Qed.

Lemma RMWAcqRaw_alloc_vs l h (φ φd: Z → vPred) V:
  Hist l h -∗ ⌜alloc_local h V⌝ -∗ ⌜uninit h⌝
  ==∗ ∃ n, ▷ RSLInv φ φd l n ∗ RelRaw V φ φ n ∗ RMWAcqRaw V n.
Proof.
  iIntros "Hist #ALLOC #UNI".
  iMod (saved_prop_alloc (φ: (Z -c> View -c> ∙)%CF iProp)) as (i) "#SΨi".

  set s: state := (mkRST rcUn ∅ h).
  iAssert (rsl_inv l φ φd s) with "[Hist SΨi]" as "Inv".
  { iFrame "Hist UNI". iExists (λ _ _ _,True%I). iSplit.
    - by rewrite /vslices big_sepS_empty.
    - iNext. by iIntros "!#" (????). }

  iMod (own_alloc (sts_auth s ∅)) as (γ) "Own".
  { apply sts_auth_valid. abstract set_solver+. }
  rewrite -sts_op_auth_frag_up.
  iDestruct "Own" as "(OwnA & #Own')".

  iModIntro. iExists _. iSplitL "Inv OwnA".
  - iExists _,_. iSplit; first auto. rewrite /sts_inv. iExists _. by iFrame.
  - iSplit; iExists _,i,γ; rewrite /sts_own; (iSplit; first auto).
    + iFrame "SΨi Own'". iFrame "ALLOC". iNext. by iApply conv_id.
    + iFrame "Own' ALLOC". by iLeft.
Qed.

Lemma RMWAcqRaw_alloc (φ φd: Z → vPred) E π V:
  nclose physN ⊆ E →
  {{{ inv physN PSInv ∗ ▷ Seen π V }}}
    (Alloc, V) @ E
  {{{ (l: loc) V' n X, RET (#l, V');
        ⌜V ⊑ V'⌝ ∗ Seen π V' ∗ Info l 1 X
      ∗ ▷ RSLInv φ φd l n ∗ RelRaw V' φ φ n ∗ RMWAcqRaw V' n }}}.
Proof.
  iIntros (? Φ) "(kI & kS) Post".
  iApply (wp_mask_mono (↑physN)); [done|]. iApply wp_fupd.
  iApply (f_alloc with "[$kI $kS]"). iNext.
  iIntros (l V' h X) "(VV' & kS' & Hist & Info & _ & Alloc & UNI)".
  iMod (RMWAcqRaw_alloc_vs l h φ φd V' with "Hist Alloc UNI") as (n) "IRA".
  iApply ("Post" $! l V' n X with "[$VV' $kS' $Info $IRA]").
Qed.

(** Inv INIT *)
Lemma AcqRaw_init_vs l h (φ φd: Z → vPred) v V (Eqh: h = {[VInj v, V]}):
  Hist l h -∗ ⌜alloc_local h V⌝ -∗ ⌜init l h⌝
  -∗ ▷ φ v V -∗ ▷ φd v V
  ==∗ ∃ n, ▷ RSLInv φ φd l n ∗ RelRaw V φ φ n ∗ AcqRaw V φ n ∗ InitRaw V n.
Proof.
  iIntros "Hist #ALLOC #INIT Hφ Hφd".
  iMod (vbox_alloc_singleton φ) as (i) "[#slice slices]".

  set s: state := (mkRST raIn {[i]} h).
  iAssert (rsl_inv l φ φd s) with "[Hist slices Hφ Hφd]" as "Inv".
  { iFrame "Hist INIT". subst h. rewrite /res /= big_sepS_singleton.
    iFrame "Hφd". iExists _. iFrame "slices".
    iNext. rewrite 2!big_sepS_singleton fn_lookup_insert. by iFrame "Hφ". }

  iMod (own_alloc (sts_auth s {[Change i]})) as (γ) "Own".
  { apply sts_auth_valid, tok_disj_state_singleton => /=.
    by apply elem_of_singleton. }
  rewrite -2!sts_op_auth_frag_up.
  iDestruct "Own" as "[[OwnA #Own1] Own2]".

  iModIntro. iExists (encode (i, γ)). iSplitL "Inv OwnA".
  { iExists _,_. iSplit; [auto|]. rewrite /sts_inv.
    iExists _. iIntros "!>". iFrame "OwnA Inv". }

  iSplitL ""; last iSplitL "Own2"; iExists s,i,γ; rewrite /sts_own.
  - iFrame "Own1 ALLOC slice". iSplit; [auto|iNext]. by iApply conv_id.
  - iExists i, _. iFrame "Own2 ALLOC slice".
    repeat iSplit; [auto|..].
    + iPureIntro. apply i_states_in. simpl. by apply elem_of_singleton.
    + iNext. by iApply conv_id.
  - iFrame "Own1". iSplit; [auto|]. iSplit; [by iLeft|].
     iPureIntro. exists (VInj v), V. subst h. by rewrite /= elem_of_singleton.
Qed.

Lemma RMWAcqRaw_init_vs l h (φ φd: Z → vPred) v V (Eqh: h = {[VInj v, V]}):
  Hist l h -∗ ⌜alloc_local h V⌝ -∗ ⌜init l h⌝
  -∗ ▷ φ v V -∗ ▷ φd v V
  ==∗ ∃ n, ▷ RSLInv φ φd l n ∗ RelRaw V φ φ n ∗ RMWAcqRaw V n ∗ InitRaw V n.
Proof.
  iIntros "Hist #ALLOC #INIT Hφ Hφd".
  iMod (saved_prop_alloc (φ: (Z -c> View -c> ∙)%CF iProp)) as (i) "#SΨi".

  set s:= mkRST rcIn ∅ h.
  iAssert (rsl_inv l φ φd s) with "[Hist Hφ Hφd]" as "Inv".
  { iFrame "Hist INIT". subst h. rewrite /res big_sepS_singleton. iFrame "Hφd".
    iSplitL "Hφ".
    - iNext.
      iApply (big_sepS_mono _ _ ({[VInj v, V]} : History) with "[Hφ]");
        [by apply subseteq_gset_filter|reflexivity
        |by rewrite big_sepS_singleton].
    - iExists (λ _ _ _,True%I). iSplit; [by rewrite /vslices big_sepS_empty|].
      iNext. by iIntros "!#" (???) "_". }

  iMod (own_alloc (sts_auth s ∅)) as (γ) "Own".
  { apply sts_auth_valid. abstract set_solver+. }
  rewrite -sts_op_auth_frag_up.
  iDestruct "Own" as "(OwnA & #Own')".

  iModIntro. iExists _. iSplitL "Inv OwnA".
  { iExists _,_. iSplit; [auto|]. rewrite /sts_inv.
    iExists _. iIntros "!>". iFrame. }

  iSplit; last iSplit; iExists s,i,γ; rewrite /sts_own;
    (iSplit; first auto); iFrame "Own'".
  - iFrame "ALLOC SΨi". iNext. by iApply conv_id.
  - iFrame "ALLOC". by iRight.
  - iSplit; [by iRight|iPureIntro].
    exists (VInj v), V. subst h. by rewrite /= elem_of_singleton.
Qed.

Lemma AcqRaw_init l v h (φ φd: Z → vPred) E π V:
  nclose physN ⊆ E →
  {{{ inv physN PSInv ∗ ▷ Seen π V
      ∗ ▷ Hist l h ∗ ⌜alloc_local h V⌝
      ∗ ▷ φ v V ∗ ▷ φd v V}}}
    (#l <~ #v, V) @ E
  {{{ V' n, RET (#(), V');
        ⌜V ⊑ V'⌝ ∗ Seen π V' ∗ ▷ RSLInv φ φd l n
        ∗ RelRaw V' φ φ n ∗ AcqRaw V' φ n ∗ InitRaw V' n }}}.
Proof.
  iIntros (? Φ) "(#kI & kS & Hist & #Alloc & Hφ & Hφd) Post".
  iApply wp_fupd. iApply (wp_mask_mono (↑physN)); first done.

  iApply (f_write_na with "[$kI $kS $Hist $Alloc]").
  iNext. iIntros (V' h') "(% & kS' & Hist' & % & _ & HInit & _ & %)".
  iDestruct (vPred_mono V V' with "Hφ") as "Hφ"; first auto.
  iDestruct (vPred_mono V V' with "Hφd") as "Hφd"; first auto.
  iMod (AcqRaw_init_vs l h' φ φd v V' with "Hist' [%] HInit Hφ Hφd")
    as (n) "(Inv & Rel & Acq & Init)"; [auto|by apply alloc_init_local|].
  by iApply ("Post" $! V' n with "[$kS' $Inv $Rel $Acq $Init]").
Qed.

Lemma RMWAcqRaw_init l v h (φ φd: Z → vPred) E π V:
  nclose physN ⊆ E →
  {{{ inv physN PSInv ∗ ▷ Seen π V
      ∗ ▷ Hist l h ∗ ⌜alloc_local h V⌝
      ∗ ▷ φ v V ∗ ▷ φd v V}}}
    (#l <~ #v, V) @ E
  {{{ V' n, RET (#(), V');
        ⌜V ⊑ V'⌝ ∗ Seen π V' ∗ ▷ RSLInv φ φd l n 
        ∗ RelRaw V' φ φ n ∗ RMWAcqRaw V' n ∗ InitRaw V' n }}}.
Proof.
  iIntros (? Φ) "(#kI & kS & Hist & #Alloc & Hφ & Hφd) Post".
  iApply wp_fupd. iApply (wp_mask_mono (↑physN)); first done.

  iApply (f_write_na with "[$kI $kS $Hist $Alloc]").
  iNext. iIntros (V' h') "(% & kS' & Hist' & % & % & HInit & % & %)".
  iDestruct (vPred_mono V V' with "Hφ") as "Hφ"; first auto.
  iDestruct (vPred_mono V V' with "Hφd") as "Hφd"; first auto.
  iMod (RMWAcqRaw_init_vs l h' φ φd v V' with "Hist' [%] HInit Hφ Hφd")
    as (n) "(Inv & Rel & RMW & Init)"; [auto|by apply alloc_init_local|].
  by iApply ("Post" $! V' n with "[$kS' $Inv $Rel $RMW $Init]").
Qed.

(** CAS *)
Lemma RMWAcqRaw_CAS
  l (φ φd: Z → vPred) (P: vPred) (R: bool → Z → vPred) (v_r v_w: Z) V n E π:
  nclose physN ⊆ E →
  □ (∀ V v, φd v V -∗ φd v V ∗ φd v V) ⊢
  {{{ (P ∗ ▷ φ v_r ∗ ▷ φd v_r ={ E }=∗ ▷ φ v_w∗ ▷ φd v_w ∗ R true v_r)%VP V
      ∗ (∀ v, ■ (v ≠ v_r) → P ∗ ▷ φd v ={ E }=∗ R false v)%VP V
      ∗ inv physN PSInv ∗ ▷ Seen π V
      ∗ ▷ RSLInv φ φd l n ∗ RMWAcqRaw V n ∗ InitRaw V n ∗ P V}}}
      (CAS #l #v_r #v_w, V) @ E
  {{{ (b: bool) (v': Z) V', RET (LitV b, V');
        ⌜V ⊑ V'⌝ ∗ Seen π V' 
        ∗ ▷ RSLInv φ φd l n ∗ RMWAcqRaw V' n ∗ InitRaw V' n ∗ R b v' V'}}}.
Proof.
  iIntros (?) "#Dup !#".
  iIntros (Φ) "(CSucc & CFail & #kI & kS & Inv & #RR & #IR & P) Post".
  iDestruct "RR" as (s1 j γ) "(% & #Own1 & RR)".
  iDestruct "RR" as %[ISCAS Alloc].
  iDestruct "IR" as (s2 j1 γ1) "(% & #Own2 & IR)". subst n.
  apply encode_inj in H1. inversion H1. subst j1 γ1. clear H1.
  iDestruct "IR" as %[ISINIT Init].
  rewrite {1}/RSLInv /sts_inv.
  iDestruct "Inv" as (j2 γ2) "(>% & Inv)".
  apply encode_inj in H0. inversion H0. subst j2 γ2. clear H0.

  iDestruct "Inv" as (s) "(>Own & Inv)".
  iDestruct (sts_auth_frag_in_up with "[$Own $Own1]") as %Up1.
  iDestruct (sts_auth_frag_in_up with "[$Own $Own2]") as %Up2.
  have Hs : rtype s = rcIn by eapply (frame_steps_RMW_init _ s1 s2 ∅ ∅).
  have HInit: init_local (rhist s) V.
  { apply (init_local_mono_rsl_state s2); [auto|].
    by apply (frame_steps_steps _ _ ∅). }
  have HAlloc: alloc_local (rhist s) V by apply alloc_init_local.
  have Init2: isInit s by apply (frame_steps_init s2 _ ∅).

  destruct s as [[] Γ h];
    [done|done|by destruct Init2|clear ISINIT ISINIT Hs]; simpl in *.
  set s := (mkRST rcIn Γ h).
  iDestruct "Inv" as "(Hist & #INIT & resD & res & slices)".

  iApply wp_fupd. iApply (wp_mask_mono (↑physN)); first auto.
  iApply (f_CAS with "[$kI $kS $Hist]"); [done|].
  iNext. iIntros (b V' h') "(% & kS' & Hist' & % & CCase)".
  iDestruct "CCase" as (V1 v) "(% & % &% & CCase)".

  (* get dup interpretation *)
  iDestruct (big_sepS_dup_later _ _ _ H3 with "[$resD Dup]") as "(Hφd & resD)".
  { iNext. by iApply "Dup". }
  iDestruct (vPred_mono V1 V' with "Hφd") as "Hφd"; [auto|].
  iDestruct (vPred_mono V V' with "P") as "P"; [auto|].

  iMod (Hist_hTime_ok with "[$kI $Hist']") as "(Hist' & HO)"; first auto.
  iDestruct "HO" as %HO.

  iDestruct "CCase"
    as "[(% & % & InV & % & DISJ & % & % & %)|(% & #NE & % & %)]".
  - iClear "CFail". subst b v h'; simpl in *.
    iDestruct "InV" as %InV. iDestruct "DISJ" as %DISJ.

    assert (Hp': ((VInj v_r), V1) ∈ gblock_ends l h).
    { apply elem_of_filter. split; [|auto] => p0 ?? [? [? ?]].
      apply H10. exists p0. split; [auto|by eexists]. }

    have Hp: (VInj v_w, V') ∉ gblock_ends l h by abstract set_solver+DISJ.

    (* ghost state update *)
    set s' := (mkRST rcIn Γ ({[VInj v_w, V']} ∪ h)).
    iMod (own_update _ _ (sts_auth s' ∅)
      with "Own") as "Own".
    { apply sts_update_auth, rtc_once.
      constructor; [by econstructor|..|done]; apply disjoint_empty_r. }
    rewrite -sts_op_auth_frag_up.
    iDestruct "Own" as "(Own & #Own')".

    (* get full interpretation *)
    rewrite /res (big_sepS_delete _ _ _ Hp').
    iDestruct "res" as "(Hφ & res)".
    iAssert (▷ (φ v_r) V')%I with "[Hφ]" as "Hφ".
    { iNext. by iApply (vPred_mono V1). }

    iMod ("CSucc" $! V' with "[] [$P $Hφ $Hφd]") as "(Hφ & Hφd & R)"; [auto|].

    iCombine "Hφd" "resD" as "resD".
    rewrite -(big_sepS_insert _ _ (VInj v_w, V')); last first.
      { by apply disjoint_singleton_r in DISJ. }
    iCombine "Hφ" "res" as "res".
    rewrite -(big_sepS_insert _ _ (VInj v_w, V')); last first.
      { move => In. by apply difference_subseteq in In. }

    iModIntro. iApply ("Post" $! _ v_r V').
    iFrame "kS' R". iSplitL ""; [auto|]. iSplitR ""; last iSplitL "".
    + rewrite /RSLInv /sts_inv.
      iExists j, γ. iSplitL ""; first by iNext.
      iExists s'. iFrame "Own Hist' slices resD".
      iNext. iSplit; [auto|]. iNext.
      iApply (big_sepS_mono _ _ _ (gblock_ends l _) with "[$res]"); [|done].
      apply gblock_ends_ins_sL_update;
        [..|by apply gblock_ends_ins_update|by apply HO]; auto.
    + iExists s',j,γ. rewrite /sts_own.
      iFrame "Own'". repeat iSplit; iPureIntro; [auto|by right|].
      by exists (VInj v_w), V'.
    + iExists s',j,γ. rewrite /sts_own. iFrame "Own'".
      repeat iSplit; iPureIntro; [auto|by right|]. by exists (VInj v_w), V'.

  - subst b h'.
    iMod ("CFail" $! v V' with "[] NE [] [$P $Hφd]") as "R"; [auto|auto|].
    rewrite -sts_op_auth_frag_up.
    iDestruct "Own" as "(Own & #Own')".

    iModIntro. iApply ("Post" $! _ v V'). iFrame "kS' R". iSplitL ""; [auto|].
    iSplitR ""; last iSplitL "".
    + rewrite /RSLInv /sts_inv. iExists j,γ.
      iSplitL ""; first auto. iExists s.
      iFrame "Own Hist' INIT resD res slices".
    + iExists s,j,γ. rewrite /sts_own. iFrame "Own'".
      repeat iSplit; iPureIntro; [auto|by right|].
      by apply (alloc_local_Mono _ _ V).
    + iExists s,j,γ. rewrite /sts_own. iFrame "Own'".
      repeat iSplit; iPureIntro; [auto|by right|].
      by apply (init_local_Mono _ _ V).
Qed.

Close Scope I.

End proof.

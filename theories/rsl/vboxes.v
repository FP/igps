From stdpp Require Export functions.
From iris.base_logic Require Export lib.saved_prop big_op.
From iris.proofmode Require Import tactics.

From igps Require Import infrastructure.
From igps.rsl Require Export gset_iops.

Set Default Proof Using "Type".
Import uPred.

Section vBoxes.
Context `{EqDecision valT} {viewT : Type}.

Class vboxG Σ := vboxG_inG :> savedPropG Σ (valT -c> viewT -c> ∙).

Definition vboxΣ : gFunctors := #[ savedPropΣ (valT -c> viewT -c> ∙) ].

Global Instance subG_vboxΣ Σ : subG vboxΣ Σ → vboxG Σ.
Proof. solve_inG. Qed.

  Section defs.
  Context `{vboxG Σ}.
  Local Notation iProp := (iProp Σ).

  Definition slice_name := gname.
  Implicit Types (Ψ: slice_name → valT → viewT → iProp)
                 (φ: valT → viewT → iProp)
                 (Γ : gset slice_name) (γ: slice_name).

  Definition sconv φ Ψ Γ : iProp
    := (□ ∀ v V, φ v V -∗ [∗ set] i ∈ Γ, Ψ i v V)%I.

  Definition vslice γ φ : iProp := saved_prop_own γ φ.
  Definition vslices Γ Ψ : iProp := ([∗ set] γ ∈ Γ, vslice γ (Ψ γ))%I.
  Definition vbox Γ φ Ψ: iProp := (▷ sconv φ Ψ Γ ∗ vslices Γ Ψ)%I.

  End defs.

  Section rules.
  Context `{vboxG Σ}.
  Local Notation iProp := (iProp Σ).
  Implicit Types (Ψ: slice_name → valT → viewT → iProp)
                 (φ: valT → viewT → iProp)
                 (Γ : gset slice_name) (γ: slice_name).

  Lemma vpred_equiv_unfold φ1 φ2:
    @uPred_internal_eq (iResUR Σ) _ (φ1 : valT -c> viewT -c> iProp) φ2
    ⊣⊢ ∀ v V, φ1 v V ≡ φ2 v V.
  Proof. rewrite ofe_funC_equivI. by setoid_rewrite ofe_funC_equivI. Qed.

  Lemma vslice_agree γ φ1 φ2:
  vslice γ φ1 -∗ vslice γ φ2 -∗ ▷ ((φ1 : valT -c> viewT -c> iProp) ≡ φ2).
  Proof.
    iIntros "s1 s2". by iDestruct (saved_prop_agree with "s1 s2") as "$".
  Qed.

  Lemma vslices_agree Γ γ φ Ψ (In: γ ∈ Γ):
    vslice γ φ -∗ vslices Γ Ψ -∗ ▷ ((φ : valT -c> viewT -c> iProp) ≡ Ψ γ).
  Proof.
    iIntros "s ss". iPoseProof (big_sepS_elem_of _ _ _ In with "ss") as "s'".
    iApply (vslice_agree with "s s'").
  Qed.

  Lemma vbox_agree Γ γ φ φ0 Ψ (In: γ ∈ Γ):
    vslice γ φ -∗ vbox Γ φ0 Ψ -∗ ▷ ((φ : valT -c> viewT -c> iProp) ≡ Ψ γ).
  Proof. iIntros "s [_ ss]". by iApply (vslices_agree with "s ss"). Qed.

  Lemma vslices_mono Γ Γ' Ψ (Le: Γ' ⊆ Γ) :
    vslices Γ Ψ -∗ vslices Γ' Ψ.
  Proof. by iApply (big_sepS_mono (λ γ, vslice γ (Ψ γ))). Qed.

  Lemma vbox_alloc: vbox ∅ (λ _ _, True)%I (λ _ _ _, True)%I.
  Proof. iSplit; [iNext; by iIntros "!#" (??) "?"|by unfold vslices]. Qed.

  Lemma vslices_insert Γ Ψ γ φ (NIn: γ ∉ Γ):
    vslice γ φ -∗ vslices Γ Ψ -∗ vslices ({[γ]} ∪ Γ) (<[γ:=φ]> Ψ).
  Proof. rewrite /vslices big_sepS_fn_insert; [iIntros "$$"|done]. Qed.

  Lemma vbox_alloc_singleton φ:
    (|==> ∃ γ, vslice γ φ ∗ vbox {[γ]} φ (<[γ := φ]> (λ _ _ _, True)))%I.
  Proof.
    iMod (saved_prop_alloc (φ : (valT -c> viewT -c> ∙)%CF iProp))
      as (γ) "#Ψγ".
    iModIntro. iExists γ. iFrame "Ψγ". iSplit.
    - iNext. unfold sconv. iIntros "!#" (??) "?".
      by rewrite big_sepS_singleton fn_lookup_insert.
    - by rewrite /vslices big_sepS_singleton fn_lookup_insert.
  Qed.

  Lemma vslices_insert2 Γ Γp Ψ φ q (Le: Γ ⊆ Γp):
     ▷?q vslices Γ Ψ
     ==∗ ∃ γ, ⌜γ ∉ Γp⌝ ∗ vslice γ φ ∗ ▷?q vslices ({[γ]} ∪ Γ) (<[γ:=φ]> Ψ).
  Proof.
    iIntros "slices".
    iMod (saved_prop_alloc_strong (φ : (valT -c> viewT -c> ∙)%CF iProp) Γp)
      as (γ) "[% #Ψγ]".
    iExists γ. iFrame "Ψγ". iIntros "{$%} !>".
    iApply (vslices_insert with "Ψγ slices"). abstract set_solver.
  Qed.

  Lemma vslice_insert Γ Γp φ1 φ2 Ψ q (Le: Γ ⊆ Γp):
    ▷?q vbox Γ φ1 Ψ ==∗ ∃ γ, ⌜γ ∉ Γp⌝ ∗
      vslice γ φ2 ∗ ▷?q vbox ({[γ]} ∪ Γ) (λ v V, φ2 v V ∗ φ1 v V) (<[γ:=φ2]> Ψ).
  Proof.
    iDestruct 1 as "[#Hsconv #slices]".
    iMod (saved_prop_alloc_strong (φ2 : (valT -c> viewT -c> ∙)%CF iProp) Γp)
      as (γ) "[% #Ψγ]".
    have ?: γ ∉ Γ by abstract set_solver.
    iModIntro; iExists γ; repeat iSplit; [auto|auto| |].
    - do 2 iNext. iIntros "!#" (v V).
      rewrite (big_sepS_fn_insert (λ _ φ, φ v V)); [|done].
      iDestruct ("Hsconv" $! v V ) as "HS". iIntros "[$ ?]". by iApply "HS".
    - iNext. by iApply (vslices_insert with "Ψγ slices").
  Qed.

  Lemma vslice_delete Γ γ φ1 φ2 Ψ q (In: γ ∈ Γ):
    vslice γ φ2 -∗ ▷?q vbox Γ φ1 Ψ -∗ ∃ φ',
      ▷?q (▷ □ (∀ v V, φ1 v V -∗ (φ' v V ∗ φ2 v V))) ∗ ▷?q vbox (Γ ∖ {[γ]}) φ' Ψ.
  Proof.
    iIntros "Sγ box". iDestruct "box" as "[#Hsconv #ss]".
    iExists (λ v V, [∗ set] γ ∈ Γ ∖ {[γ]}, (Ψ γ) v V)%I.
    iNext. iDestruct (vslices_agree with "Sγ ss") as "#Eq"; [done|].
    iSplit; [|iSplit].
    - iNext. iIntros "!#" (v V).
      rewrite (sep_comm _ (φ2 v V)). rewrite vpred_equiv_unfold.
      iRewrite ("Eq" $! v V). by rewrite -big_sepS_delete.
    - iNext. by iIntros "!#" (v V) "?".
    - iApply (vslices_mono with "ss"). abstract set_solver+.
  Qed.

  Lemma vbox_pred_mono Γ φ1 φ2 Ψ:
    □ ▷ (∀ v V, φ2 v V -∗ φ1 v V) -∗ vbox Γ φ1 Ψ -∗ vbox Γ φ2 Ψ.
  Proof.
    iIntros "#conv box". iDestruct "box" as "[#Hsconv slices]".
    iFrame "slices". iNext. unfold sconv.
    iIntros "!#" (v V). iDestruct ("Hsconv" $! v V) as "#HS".
    iIntros "H2". iApply "HS". by iApply "conv".
  Qed.

  Lemma vslice_split Γ γ φ0 φ φ1 φ2 Ψ q (In: γ ∈ Γ):
    vslice γ φ0
    -∗ ▷?q (▷ □ (∀ v V, φ0 v V -∗ (φ1 v V ∗ φ2 v V)))
    -∗ ▷?q vbox Γ φ Ψ
    ==∗ ∃ γ1 γ2, ⌜γ1 ∉ Γ ∧ γ2 ∉ Γ ∧ γ1 ≠ γ2⌝
        ∗ vslice γ1 φ1 ∗ vslice γ2 φ2
        ∗ ▷?q vbox (([γ] => [γ1,γ2]) Γ) φ (<[γ1:=φ1]> (<[γ2:=φ2]> Ψ)).
  Proof.
    iIntros "s #conv box".
    iDestruct (vslice_delete with "s box") as (φ') "(#Eq & box)"; [done|].
    iMod (vslice_insert _ Γ _ φ2 with "box") as (γ2) "(H2 & #s2 & box)";
      [abstract set_solver|].
    iMod (vslice_insert _ ({[γ2]} ∪ Γ) _ φ1 with "box") as (γ1) "(H1 & #s1 & box)";
      [abstract set_solver|].
    iDestruct "H1" as %Hin1. iDestruct "H2" as %Hin2.
    iExists γ1, γ2. iIntros "{$#} !>". iSplit; [iPureIntro|].
    - repeat split; [abstract set_solver|done|abstract set_solver].
    - iNext. rewrite union_assoc_L. iApply (vbox_pred_mono with "[] box").
      iNext. iIntros "!#" (v V) "Hφ".
      iDestruct ("Eq" with "Hφ") as "[$ Hφ0]". by iApply "conv".
  Qed.

  Lemma vslice_merge Γ φ φ1 φ2 φ0 γ1 γ2 Ψ q
    (NEq: γ1 ≠ γ2) (In1: γ1 ∈ Γ) (In2: γ2 ∈ Γ):
    vslice γ1 φ1 -∗ vslice γ2 φ2
    -∗ ▷?q (▷ □ (∀ v V, φ1 v V ∗ φ2 v V -∗ φ0 v V))
    -∗ ▷?q vbox Γ φ Ψ
    ==∗ ∃ γ, ⌜γ ∉ Γ⌝ ∗ vslice γ φ0 ∗ ▷?q vbox (([γ1,γ2] => [γ]) Γ) φ (<[γ:=φ0]> Ψ).
  Proof.
    iIntros "s1 s2 #conv box".
    iDestruct (vslice_delete with "s1 box") as (φ') "(#Eq1 & box)"; [done|].
    iDestruct (vslice_delete with "s2 box") as (φ'') "(#Eq2 & box)";
      [abstract set_solver|].
    iMod (vslice_insert _ Γ _ φ0 with "box")
      as (γ) "(Hγ & #s & box)"; [abstract set_solver|].
    iDestruct "Hγ" as %In. iModIntro. iExists γ. iFrame (In) "s".
    iNext.
    rewrite (_: {[γ]} ∪ Γ ∖ {[γ1]} ∖ {[γ2]} = ([γ1,γ2] => [γ]) Γ); last first.
    { apply leibniz_equiv. by rewrite difference_twice_union. }
    iApply (vbox_pred_mono with "[] box").
    iNext. iIntros "!#" (v V) "H".
    iDestruct ("Eq1" $! v V with "H") as "[H' ?]".
    iDestruct ("Eq2" $! v V with "H'") as "[$ ?]".
    iApply "conv". by iFrame.
  Qed.

  Lemma vslice_split_value Γ γ φ φ0 Ψ (v : valT) q (In: γ ∈ Γ):
    vslice γ φ0 -∗ ▷?q vbox Γ φ Ψ ==∗
    ∃ γ1 γ2, ⌜γ1 ∉ Γ ∧ γ2 ∉ Γ ∧ γ1 ≠ γ2⌝
      ∗ vslice γ1 (<[v:= λ _ : viewT, True]> φ0)
      ∗ vslice γ2 (<[v:= φ0 v]> (λ _ _, True))
      ∗ ▷?q vbox (([γ] => [γ1,γ2]) Γ) φ
                 ( <[γ1 := <[v:= λ _ : viewT, True]> φ0]>
                  (<[γ2 := <[v:= φ0 v]> (λ _ _, True)  ]> Ψ)).
  Proof.
    iIntros "slice". iApply (vslice_split with "slice"); [done|].
    iNext. iNext. iIntros "!#" (v' V).
    case (decide (v' = v)) => [->|?].
    - rewrite 2!fn_lookup_insert. by iIntros "$".
    - do 2 (rewrite fn_lookup_insert_ne; [|done]). by iIntros "$".
  Qed.

  Lemma vslice_switch_drop_value Γ γ φ0 φ Ψ (v : valT) q (In: γ ∈ Γ):
    vslice γ φ0 -∗ ▷?q vbox Γ φ Ψ ==∗
    ∃ γ', ⌜γ' ∉ Γ⌝
      ∗ vslice γ' (<[v:= λ _ : viewT, True]> φ0)
      ∗ ▷?q vbox (([γ] => [γ']) Γ) φ (<[γ' := <[v:= λ _ : viewT, True]> φ0]> Ψ).
  Proof.
    iIntros "s box".
    iDestruct (vslice_delete with "s box") as (φ') "(#Eq & box)"; [done|].
    iMod (vslice_insert _ Γ with "box") as (γ') " (Hγ' & s' & box)";
      [abstract set_solver|].
    iExists γ'. iModIntro. iFrame "Hγ' s'". iNext.
    iApply (vbox_pred_mono with "[] box").
    iNext. iIntros "!#" (v' V) "H".
    case (decide (v' = v)) => [->|?].
    - rewrite fn_lookup_insert. by iDestruct ("Eq" with "H") as "[$ ?]".
    - rewrite fn_lookup_insert_ne; [|done].
      by iDestruct ("Eq" with "H") as "[$ $]".
  Qed.
  End rules.
End vBoxes.

Arguments vboxG : clear implicits.
Arguments vboxΣ : clear implicits.

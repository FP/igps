From stdpp Require Export gmap.
Set Default Proof Using "Type".

Section gset_iops.
  Context `{Countable A}.
  Implicit Types (i : A) (Γ: gset A).

  (* Merge *)
  Definition gset_imerge i1 i2 i Γ := {[i]} ∪ Γ ∖ {[i1; i2]}.
  (* Split *)
  Definition gset_isplit i1 i2 i Γ := {[i1; i2]} ∪ Γ ∖ {[i]}.
  (* Switch *)
  Definition gset_iswitch i i' Γ := {[i']} ∪ Γ ∖ {[i]}.

  Global Instance gset_ijoin_subseteq i1 i2 i :
    Proper ((⊆) ==> (⊆)) (gset_imerge i1 i2 i).
  Proof. intros ???. by apply union_mono_l, difference_mono_r. Qed.
  Global Instance gset_isplit_subseteq i i1 i2:
    Proper ((⊆) ==> (⊆)) (gset_isplit i1 i2 i).
  Proof. intros ???. by apply union_mono_l, difference_mono_r. Qed.
  Global Instance gset_iswitch_subseteq i i' :
    Proper ((⊆) ==> (⊆)) (gset_iswitch i i').
  Proof. intros ???. by apply union_mono_l, difference_mono_r. Qed.
End gset_iops.

Notation "[ i1 , i2 ] => [ i ]" := (gset_imerge i1 i2 i)
  (at level 200, format "[ i1 , i2 ]  =>  [ i ]") : C_scope.
Notation "[ i ] => [ i1 , i2 ]" := (gset_isplit i1 i2 i)
  (at level 200, format "[ i ]  =>  [ i1 , i2 ]") : C_scope.
Notation "[ i ] => [ i' ]" := (gset_iswitch i i')
  (at level 200, format "[ i ]  =>  [ i' ]") : C_scope.

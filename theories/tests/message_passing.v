From igps.examples Require Import message_passing unit_token.
From igps.gps Require Import shared inst_shared.
From igps Require Import adequacy.

Section MP.

Let Σ : gFunctors := #[ baseΣ;
                        gpsΣ protocols.boolProtocol _ ;
                        gps_agreeΣ protocols.boolProtocol _;
                        absViewΣ;
                        persistorΣ;
                        uTokΣ ].

Program Definition initial_state :=
  {| mem := mkMemory ∅ (_);
     nats := ∅ |}.
Next Obligation.
  rewrite /pairwise_disj. set_solver.
Qed.

Lemma message_passing :
  adequate (message_passing, ∅) initial_state (λ v, v.1 = LitV (LitInt 37)).
Proof.
  apply (base_adequacy Σ _ ∅); auto.
  - split; repeat red; rewrite /initial_state /=; try set_solver.
  - move => ? ?. done.
  - iIntros (? ?) "[#PS Seen]".
    iApply fupd_wp.
    iApply fupd_mask_mono;
    last iMod (persistor_init with "PS") as (?) "#Pers".
    { by auto. }
    iModIntro.
    iApply (message_passing_spec ∅ with "[%] PS Seen Pers").
    { reflexivity. }
    iNext. iViewUp. iIntros (v) "? %". by subst.
Qed.

Print Assumptions message_passing.

End MP.
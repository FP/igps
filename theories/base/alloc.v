From igps Require Import lang.
From iris.proofmode Require Import tactics.
From igps.base Require Import ghosts at_shared.

Section Alloc.
  Context `{fG : !foundationG Σ}.
  Local Notation iProp := (iProp Σ).
  Local Set Default Proof Using "Σ fG".

  Implicit Types (π ρ : thread) (l : loc) (V : View) (h : History).

  Lemma Hist_Info_fresh
    ς HIST INFO l
    (H1 : HInv ς HIST) (H2 : IInv ς INFO):
    HIST !! l = None ↔ INFO !! l = None.
  Proof.
    destruct H1 as [H1 _]. cbn in H2.
    rewrite <- H1 in H2.
    specialize (H2 l).
    rewrite -(not_elem_of_dom (D:= (gset loc)) HIST).
    rewrite -(not_elem_of_dom (D:= (gset loc)) INFO).
    abstract naive_solver.
  Qed.

  Lemma IInv_update_alloc
    ς ς' INFO l m v
    (II1: IInv ς INFO)
    (IO: INFO !! l = None)
    (Disj : MS_msg_disj (msgs (mem ς)) m)
    (MUpd : mem ς' = add_ins (mem ς) m Disj)
    (EqLoc : mloc m = l)
    : IInv ς' (<[l:=v]> INFO).
  Proof.
    cbn in *.
    rewrite dom_insert MUpd II1 /add_ins /=.
    move: IO. rewrite -(not_elem_of_dom (D:= (gset loc)) INFO).
    abstract set_solver+II1 EqLoc.
  Qed.



  Local Coercion of_Z := LitV ∘ LitInt.

  (* Lemma alloc_update_HInv *)
  (*   ς ς' l m HIST V *)
  (*   (Inv: HInv ς HIST) *)
  (*   (Disj : MS_msg_disj (msgs (mem ς)) m) *)
  (*   (MUpd : mem ς' = add_ins (mem ς) m Disj) *)
  (*   (NAUpd : nats ς' = <[l:=mtime m]> (nats ς)) *)
  (*   (EqLoc : mloc m = l) *)
  (*   (EqV : mview m = V) *)
  (*   (EqVal: mval m = A) *)
  (*   (OK: msg_ok m) *)
  (*   (Fresh: ∀ m : message, m ∈ msgs $ mem ς → mloc m ≠ l) *)
  (*   : HInv ς' (<[l:=Excl {[A, V]}]> HIST). *)
  (* Proof. *)
  (*   split. *)
  (*   - rewrite dom_insert (proj1 Inv) MUpd /=. abstract set_solver+EqLoc. *)
  (*   - move => l0 V0. *)
  (*     case: (decide (l = l0)) => [<- {l0}|NEq]. *)
  (*     + rewrite lookup_insert => [[<- {V0}]]. *)
  (*       exists (A,V). apply set_unfold_2. repeat split. *)
  (*       * abstract naive_solver. *)
  (*       * by rewrite NAUpd lookup_insert -EqLoc -EqV OK. *)
  (*       * move: x => [? ?] [-> ->]. exists m. split; first by rewrite EqVal EqV. *)
  (*         repeat split; [by rewrite -EqLoc -EqV OK|auto|]. *)
  (*         rewrite MUpd. apply set_unfold_2. by right. *)
  (*       * move: x => [? ?] [m' [[? ?]]]. subst. rewrite MUpd. *)
  (*         apply set_unfold_2. *)
  (*         move => [_ [EqLoc [Inm'|->]]]; last by rewrite EqVal. *)
  (*         abstract naive_solver. *)
  (*       * abstract naive_solver. *)
  (*     + rewrite /LInv NAUpd !lookup_insert_ne; [|auto|auto]. *)
  (*       move/(proj2 Inv) => [m' [H1 [H2 [H3 [H H4]]]]]. *)
  (*       exists m'. repeat split; auto; rewrite MUpd H. *)
  (*       * rewrite /map_vt /hist_from_opt. abstract set_solver+. *)
  (*       * abstract set_solver. *)
  (* Qed. *)


  Lemma f_alloc π V:
    {{{ PSCtx ∗ ▷ Seen π V }}}
      (Alloc, V) @ ↑physN
    {{{ l V' h' v, RET (#l, V');
          ⌜V ⊑ V'⌝ ∗ Seen π V' ∗ Hist l h' ∗ Info l 1 v
        ∗ ⌜na_safe l h' V'⌝  ∗ ⌜alloc_local h' V'⌝ ∗ ⌜uninit h'⌝ }}}.
  Proof.
    iIntros (Φ) "[#? HSeen] Post".

    iMod (PSInv_open_alloc with "[-Post]")
      as (ς HIST INFO) "(oPS & % & % & % & % & oAI & HClose)" ; auto.
    iApply (wp_alloc_pst with "oPS"); eauto.
    iNext. iFrame. iIntros (l ς' V') "(% & % & % & % & ownς')".
    inversion H6.

    assert (INone: INFO !! l = None).
    { rewrite <- (not_elem_of_dom (D:= (gset loc)) INFO).
      rewrite H2. abstract set_solver+Fresh. }

    (* iDestruct (own_valid with "oAH") as %[_ HValid]%auth_valid_discrete. *)
    (* assert (HNone: HIST !! l = None). *)
    (*   { by rewrite Hist_Info_fresh. } *)

    pose v := DecAgree 1%positive.

    iMod ((Infos_alloc _ _ v) with "oAI") as "oAI"; [eauto|done|].

    iMod ("HClose" $! l v ς' V' (mtime m) with "[-Post]") as "[? [? ?]]".
      { iFrame (H3 H4 H5) "∗".
        iSplit; [|iSplit; [|iSplit; [|iSplit]]]; try iPureIntro.
        - by apply/Hist_Info_fresh.
        - rewrite MUpd /=. destruct m. cbn in *. subst. done.
        - by rewrite NATSOld.
        - rewrite /LInv.
          eexists (_,_); split; [|split; [|split; [|split]]].
          + rewrite elem_of_singleton. reflexivity.
          + move => ?. rewrite elem_of_singleton => <- //.
          + rewrite NATSOld lookup_insert VUpd infrastructure.lookup_join lookup_singleton /=.
            solve_jsl.
          + move => ?. rewrite elem_of_singleton.
            split.
            * move => -> /=. rewrite MUpd /=.
              apply/elem_of_map_gset. eexists (<l → A@mtime m,_>). split; first reflexivity.
              apply/elem_of_filter; split; last first.
              { apply/elem_of_hist; split; first reflexivity.
                apply/elem_of_union. right. rewrite elem_of_singleton.
                destruct m. cbn in *. by subst.
              }
              rewrite VUpd infrastructure.lookup_join lookup_singleton Local. solve_jsl.
            * move/elem_of_map_gset => [m' [->]].
              move/elem_of_filter => [?]. move/elem_of_hist => [EqLoc'].
              rewrite MUpd /= elem_of_union => [] [In|].
              { exfalso. move: (Fresh m').
                rewrite elem_of_filter -EqLoc' => /=. by set_solver+In. }
              rewrite elem_of_singleton => ->. by rewrite -EqVal -EqView.
          + set_solver+.
        - apply: (IInv_update_alloc _ _ _ _ _ _ _ _ Disj); try done.
      }

    iApply ("Post" $! l V' _ v). iFrame. iPureIntro.
    repeat split.
    - rewrite VUpd. solve_jsl.
    - repeat eexists; first exact/elem_of_singleton; last reflexivity.
      move => [? ?] /elem_of_singleton [_ ->] //=.
    - repeat eexists; first exact/elem_of_singleton; eauto. solve_jsl.
    - by eexists.
  Qed.
End Alloc.